package com.suyundeng.controller;

import com.suyundeng.entity.JsonResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class MyControllerExceptionHandler {

	@ResponseBody
	@ExceptionHandler(Exception.class)
	public JsonResult handleException(Exception ex){
		return new JsonResult(0,ex.getMessage());
	}
}
