package com.suyundeng.controller;

import com.suyundeng.entity.JsonResult;

import com.suyundeng.utils.FtpUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.util.UUID;

@RestController
public class UploadController {
    @PostMapping("/up/upload")
    public JsonResult toUpload(@RequestParam("file") MultipartFile file) throws Exception {
        String newFileName="";
        if (file!=null){
            String originalFilename = file.getOriginalFilename();
            String substring = originalFilename.substring(originalFilename.indexOf("."));
            FileInputStream fs = (FileInputStream) file.getInputStream();
            FtpUtil.getConnect();
            newFileName = UUID.randomUUID().toString().replace("-", "").substring(0, 10) + substring;
            FtpUtil.uploadFile(fs, newFileName);
            FtpUtil.close();
        }
        return new JsonResult(1,newFileName);
    }
}
