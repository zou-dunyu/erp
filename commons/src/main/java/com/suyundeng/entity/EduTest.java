package com.suyundeng.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName edu_test
 */
@Data
public class EduTest implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String test;

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    private Integer age;

    /**
     * 
     */
    private String address;

    /**
     * 
     */
    private String tel;

    private static final long serialVersionUID = 1L;
}