package com.suyundeng.entity;

import lombok.Data;

import java.io.Serializable;
/**
 * 用于封装服务器到客户端的Json返回值
 * @author linzhenbing
 *
 */
@Data
public class JsonResult<T> implements Serializable {
	//Serializable将对象的状态保存在存储媒体中以便可以在以后重新创建出完全相同的副本
	public static final int SUCCESS=0;
	public static final int ERROR=1;
	public static final int OTHER=2;

	/**
	 * 返回是否成功，1成功，0失败
	 */
	private int state;

	/**
	 * 返回提示消息
	 */
	private String message = "";

	/**
	 * 返回数据
	 */
	private T data;

	/**
	 * 返回错误消息
	 */
	private String pass="";

	public JsonResult(){
		state = SUCCESS;
	}
	//为了方便，重载n个构造器
	public JsonResult(int state, String message, T data) {
		this.state = state;
		this.message = message;
		this.data = data;
	}
	public JsonResult(int state,String error){
		this(state,error,null);
	}
	public JsonResult(int state,T data){
		this(state,"",data);
	}
	public JsonResult(String error){
		this(ERROR,error,null);
	}

	public JsonResult(T data){
		this(SUCCESS,"",data);
	}
	public JsonResult(int state){
		this(state,"",null);
	}
	public JsonResult(Throwable e){
		this(ERROR,e.getMessage(),null);
	}

}
