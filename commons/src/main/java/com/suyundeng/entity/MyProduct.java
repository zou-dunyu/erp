package com.suyundeng.entity;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
public class MyProduct implements Serializable {

    private String id;

    private String pNumber;

    private String pName;

    private String wareName;

    private String goodName;

    private String shelvesNo;

    private String cargoNo;

    private static final long serialVersionUID = 1L;

}
