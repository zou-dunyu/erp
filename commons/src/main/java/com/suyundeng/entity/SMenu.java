package com.suyundeng.entity;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * 
 * @TableName s_menu
 */
@Data
@JsonIgnoreProperties(value = "handler")
public class SMenu implements Serializable {
    /**
     * 菜单id
     */
    private String id;

    /**
     * 层级
     */
    private String menuPid;

    /**
     * 菜单名字
     */
    private String menuName;

    /**
     * 地址
     */
    private String menuUrl;

    /**
     * 权限
     */
    private String menuPermission;

    /**
     * 子菜单
     */
    @TableField(exist = false)
    private List<SMenu> menusSon;

    private static final long serialVersionUID = 1L;
}