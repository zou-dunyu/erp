package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName s_role
 */
@Data
public class SRole implements Serializable {
    /**
     * 角色id
     */
    private Integer id;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String roleDescribe;

    /**
     * 创建人

     */
    private String roleCreateName;

    /**
     * 创建时间

     */
    private Date roleCreateDate;

    /**
     * 状态

     */
    private String roleStatus;

    /**
     * 公司id

     */
    private String companyId;

    /**
     * 修改人

     */
    private String roleUpdateName;

    /**
     * 修改时间

     */
    private Date roleUpdateDate;

    private static final long serialVersionUID = 1L;
}