package com.suyundeng.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName s_role_menu
 */
@Data
public class SRoleMenu implements Serializable {
    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 菜单id
     */
    private String menuId;

    private static final long serialVersionUID = 1L;
}