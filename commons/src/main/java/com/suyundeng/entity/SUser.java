package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName s_user
 */
@Data
public class SUser implements Serializable {
    /**
     * 用户id
     */
    private String id;

    /**
     * 用户名(登录帐号)
     */
    private String userName;

    /**
     * 用户密码(初始密码)
     */
    private String passWord;

    /**
     * 混淆数
     */
    private String salt;

    /**
     * 头像
     */
    private String userImage;

    /**
     * 真实姓名(员工姓名)
     */
    private String realName;

    /**
     * 性别
     */
    private String sex;

    /**
     * 生日年份
     */
    private String birYear;

    /**
     * 生日月份
     */
    private String birMonth;

    /**
     * 生日日期
     */
    private String birDay;

    /**
     * 个人邮箱
     */
    private String userEmail;

    /**
     * 联系方式
     */
    private String userTel;

    /**
     * 个人状态(账号状态)
     */
    private String userStatus;

    /**
     * 公司id(企业id)
     */
    private String companyId;

    /**
     * 查询次数
     */
    private Integer queryTimes;

    /**
     * 入职时间
     */
    private Date entryTime;

    /**
     * 员工编号
     */
    private String no;

    /**
     * 员工成绩
     */
    private String grade;

    /**
     * 员工状态
     */
    private String status;

    /**
     * 职位id
     */
    private String pId;

    /**
     * 所属部门id
     */
    private String dId;

    private static final long serialVersionUID = 1L;
}