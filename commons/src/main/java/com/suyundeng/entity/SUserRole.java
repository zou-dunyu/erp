package com.suyundeng.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName s_user_role
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SUserRole implements Serializable {
    /**
     * 用户id
     */
    private String userId;

    /**
     * 角色id
     */
    private String roleId;

    private static final long serialVersionUID = 1L;
}