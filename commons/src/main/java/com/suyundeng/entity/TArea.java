package com.suyundeng.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 地区码表
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TArea implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 地区Id
     */
    @TableId(value = "areaId", type = IdType.AUTO)
    private Integer areaId;

    /**
     * 地区编码
     */
    @TableField("areaCode")
    private String areaCode;

    /**
     * 地区名
     */
    @TableField("areaName")
    private String areaName;

    /**
     * 地区级别（1:省份province,2:市city,3:区县district,4:街道street）
     */
    private Integer level;

    /**
     * 城市编码
     */
    @TableField("cityCode")
    private String cityCode;

    /**
     * 城市中心点（即：经纬度坐标）
     */
    private String center;

    /**
     * 地区父节点
     */
    @TableField("parentId")
    private Integer parentId;


}
