package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 
 * @TableName t_cargo
 */
@Data
public class TCargo implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String cargoNo;

    /**
     * 
     */
    private String shelvesId;

    /**
     * 
     */
    private String damId;

    /**
     * 
     */
    private String wareId;

    /**
     * 
     */
    private String productNo;

    /**
     * 
     */
    private String product;

    /**
     * 
     */
    private Date creatTime;

    /**
     * 
     */
    private Integer batch;

    /**
     * 
     */
    private String status;

    /**
     * 
     */
    private String iomNo;

    /**
     * 
     */
    private String companyId;

    /**
     * 
     */
    private String lCode;

    /**
     * 
     */
    private String remark;

    /*
    仓库实体类
    * */
    @TableField(exist = false)
    private TWare tWare;

    /*
    * 库区实体类
    * */
    @TableField(exist = false)
    private TCargoArea tCargoArea;

    /*
    货架实体类
    * */
    @TableField(exist = false)
    private TShelves tShelves;

    private static final long serialVersionUID = 1L;
}