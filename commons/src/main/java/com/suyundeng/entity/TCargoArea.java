package com.suyundeng.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName t_cargo_area
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TCargoArea implements Serializable {
    public TCargoArea(String cargoAreaName, String wareId, String settledDate) throws ParseException {
        this.goodsName = cargoAreaName;
        this.wareId = wareId;
        if (settledDate.length() > 4) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            this.deteTime1 = simpleDateFormat.parse(settledDate);
        }
    }

    /**
     * id
     */
    private String id;

    /**
     * 货区编号
     */
    private String goodsNo;

    /**
     * 货区名称
     */
    private String goodsName;

    /**
     * 所属仓库id
     */
    private String wareId;

    /**
     * 所属负责人id
     */
    private String personId;

    /**
     * 货架数目
     */
    private Integer shelvesNum;

    /**
     * 创建时间
     */

    private Date deteTime1;

    /**
     * 面积
     */
    private String area;

    /**
     * 状态
     */
    private String status;

    /**
     * 货区描述
     */
    private String details;

    /**
     * 所属公司id
     */
    private String companyId;

    /*
     * 仓库表
     * */
    @TableField(exist = false)//忽略该数据，添加数据库是不添加该数据
    private TWare tWare;
    /*
     * 货架表
     * */
    @TableField(exist = false)//忽略该数据，添加数据库是不添加该数据
    private TShelves tShelves;

    /*
     * 下属货架集合
     * */
    @TableField(exist = false)
    List<TShelves> shelves;

    private static final long serialVersionUID = 1L;
}