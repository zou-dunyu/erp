package com.suyundeng.entity;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 
 * @TableName t_categories
 */
@Data
public class TCategories implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 父级菜单id
     */
    private String pId;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 添加人
     */
    private String userId;

    /**
     * 分类编号
     */
    private Integer mNumber;

    /**
     * 分类介绍
     */
    private String mIntroduction;

    /**
     * 公司id
     */
    private String companyId;

    private static final long serialVersionUID = 1L;

    //子菜单
    @TableField(exist =  false)
    List<TCategories> subProducts;



}