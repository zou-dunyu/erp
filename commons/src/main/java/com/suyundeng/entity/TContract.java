package com.suyundeng.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;

/**
 * <p>
 * 
 * </p>
 *
 * @author wang
 * @since 2021-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TContract implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private String id;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 签订时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date dateOfSigning;

    /**
     * 到期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date dueDate;

    /**
     * 合同名称
     */

    private String name;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 甲方
     */
    private Integer first;

    /**
     * 乙方
     */
    private Integer second;
}
