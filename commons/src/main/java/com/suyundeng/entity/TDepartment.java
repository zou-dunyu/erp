package com.suyundeng.entity;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 
 * @TableName t_department
 */
@Data
public class TDepartment implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 父级菜单
     */
    private String pid;

    /**
     * 部门编号
     */
    private String no;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 部门经理
     */
    private String manager;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 固定电话
     */
    private String tel;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 部门简介
     */
    private String intro;

    /**
     * 企业id
     */
    private String cId;

    @TableField(exist = false)
    List<TDepartment> subTDepartment;

    @TableField(exist = false)
    List<TPost>  post ;

    private static final long serialVersionUID = 1L;
}