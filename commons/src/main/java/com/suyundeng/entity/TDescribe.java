package com.suyundeng.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName t_describe
 */
@Data
public class TDescribe implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String statusDescribe;

    /**
     * 
     */
    private String describeValue;

    /**
     * 
     */
    private String statusName;

    private static final long serialVersionUID = 1L;
}