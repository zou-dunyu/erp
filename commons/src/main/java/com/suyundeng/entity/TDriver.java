package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName t_driver
 */
@Data
public class TDriver implements Serializable {
    /**
     * 
     */
    private String driverId;

    /**
     * 
     */
    private String driverName;

    /**
     * 
     */
    private String driverLicense;

    /**
     * 
     */
    private String idCard;

    /**
     * 
     */
    private String driverSex;

    /**
     * 
     */
    private String driverControl;

    /**
     * 
     */
    private String logisticsId;

    /**
     * 
     */
    private String driverAge;

    /**
     * 
     */
    private String remarks;

    /**
     * 
     */
    private String status;

    /**
     * 
     */
    private String createId;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private String updateId;

    /**
     * 
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}