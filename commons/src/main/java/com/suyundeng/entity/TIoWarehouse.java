package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.*;

/**
 * 
 * @TableName t_io_warehouse
 */
@Data
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class TIoWarehouse implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 出入库单号
     */
    private String ioNo;

    /**
     * 出入库类型
     */
    private String ioType;

    /**
     * 批次
     */
    private Integer batch;

    /**
     * 产品名称
     */
    private String pName;

    /**
     * 产品类型
     */
    private String pBrand;



    /**
     * 产品数量
     */
    private Integer pNum;

    /**
     * 仓库名称
     */
    private String wName;

    /**
     * 出入库时间
     */
    private Date ioTime;

    /**
     * 公司ID
     */
    private String companyId;

    private static final long serialVersionUID = 1L;



}