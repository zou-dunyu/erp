package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName t_iom_warehouse
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TIomWarehouse implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String iomGoodsno;

    /**
     * 
     */
    private String iomGoods;

    /**
     * 
     */
    private String iomName;

    /**
     * 
     */
    private String iomArea;

    /**
     * 
     */
    private String iomShelves;

    /**
     * 
     */
    private String iomCargoloca;

    /**
     * 
     */
    private Date iomTime;

    /**
     * 
     */
    private String iomNo;

    private static final long serialVersionUID = 1L;



}