package com.suyundeng.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName t_laser
 */
@Data
public class TLaser implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 订单编号
     */
    private String lNo;

    /**
     * 激光码
     */
    private String lCode;

    /**
     * 激光码状态  -1待打印 1已打印
     */
    private String lStatus;

    /**
     * 打印机
     */
    private String lPrinter;

    /**
     * 序列号
     */
    private String lSerial;

    /**
     * 公司ID
     */
    private String companyId;

    /*
     *商品类型名称
     */
    @TableField(exist = false)
    private String name;

    /**
     * 商品名称
     */
    @TableField(exist = false)
    private String pName;

    /*
    *订单ID
    * */
    @TableField(exist = false)
    private String orderId;

    /*
     *交易时间
     * */
    @TableField(exist = false)
    private String dealTime;

    /*
     *交易金额
     * */
    @TableField(exist = false)
    private String money;

    /*
     *交易数量
     * */
    @TableField(exist = false)
    private String number;

    private static final long serialVersionUID = 1L;
}