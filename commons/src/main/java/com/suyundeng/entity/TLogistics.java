package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName t_logistics
 */
@Data
public class TLogistics implements Serializable {
    /**
     * 
     */
    private String logisticsId;

    /**
     * 
     */
    private String logisticsName;

    /**
     * 
     */
    private String principal;

    /**
     * 
     */
    private String logisticsContact;

    /**
     * 
     */
    private String area;

    /**
     * 
     */
    private Date joinTime;

    /**
     * 
     */
    private Integer price;

    /**
     * 
     */
    private String remarks;

    /**
     * 
     */
    private String createId;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private String updateId;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 
     */
    private String companyId;

    private static final long serialVersionUID = 1L;
}