package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @TableName t_move_warehouse
 */
@Data
@AllArgsConstructor
public class TMoveWarehouse implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String mvNo;

    /**
     * 
     */
    private String mvOut;

    /**
     * 
     */
    private String mvIn;

    /**
     * 
     */
    private String mvGoods;

    /**
     * 
     */
    private Integer mvNumber;

    /**
     * 
     */
    private String mvMark;

    /**
     * 
     */
    private String mvPerson;

    /**
     * 
     */
    private String mvTel;

    /**
     * 
     */
    private Date mvTime;

    /**
     * 
     */
    private String companyId;

    private static final long serialVersionUID = 1L;


}