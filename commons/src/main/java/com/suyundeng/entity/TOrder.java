package com.suyundeng.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName t_order
 */
@Data
@NoArgsConstructor
public class TOrder implements Serializable {

    public TOrder(String id, String orderId, String orderType, String productId, String customId, String payType, Double money, Date dealTime, Integer number, String orderStatus, String supplierId, String orderHolder) {
        this.id = id;
        this.orderId = orderId;
        this.orderType = orderType;
        this.productId = productId;
        this.customId = customId;
        this.payType = payType;
        this.money = money;
        this.dealTime = dealTime;
        this.number = number;
        this.orderStatus = orderStatus;
        this.supplierId = supplierId;
        this.orderHolder = orderHolder;
    }

    /**
     * 订单id
     */
    private String id;

    /**
     * 订单编号
     */
    private String orderId;

    /**
     * 订单类型
     */
    private String orderType;

    /**
     * 产品id
     */
    private String productId;

    /**
     * 付款公司id
     */
    private String customId;

    /**
     * 支付方式
     */
    private String payType;

    /**
     * 交易金额
     */
    private Double money;

    /**
     * 交易时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date dealTime;

    /**
     * 数量
     */
    private Integer number;

    /**
     * 订单状态 1，2,3,4
     */
    private String orderStatus;

    /**
     * 说明
     */
    private String explains;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 生产计划编号
     */
    private String productPlanNo;

    /**
     * 责任产线id
     */
    private String productLineId;

    /**
     * 计划生产日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date productPlanTime;

    /**
     * 计划结束生产日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date productPlanOvertime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 生产状态5,6,7,8,9,10
     */
    private String productStatus;

    /**
     * 打印机
     */
    private String printer;

    /**
     * 生产时间
     */
    private Date productTime;

    /**
     * 激光码
     */
    private String laserCode;

    /**
     * 发货日期
     */
    private Date shippingDate;

    /**
     * 发货方式的id
     */
    private String shippingMethodId;

    /**
     * 接受订单公司id
     */
    private String supplierId;

    /**
     * 下单者
     */
    private String orderHolder;

    /**
     * 审核时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date auditTime;

    /**
     * 客户公司名称
     */
    @TableField(exist = false)
    private String companyName;

    /**
     * 供应商公司名称
     */
    @TableField(exist = false)
    private String supplierName;

    /**
     * 商品名称
     */
    @TableField(exist = false)
    private String pName;

    /**
     * 商品编号
     */
    @TableField(exist = false)
    private String pNumber;

    /*
    *商品分类
    * */
    @TableField(exist = false)
    private String pEnterprise;

    /*
    * 生产产线
    * */
    @TableField(exist = false)
    private String productionName;

    /*
    * 激光码状态
    * */
    @TableField(exist = false)
    private String lStatus;

    /**
     * 订单状态
     */
    @TableField(exist = false)
    private String allStatus;

    /**
     * 对应商品
     */
    @TableField(exist = false)
    private TProduct tProduct;



    private static final long serialVersionUID = 1L;
}