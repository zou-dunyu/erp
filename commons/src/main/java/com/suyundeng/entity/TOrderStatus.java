package com.suyundeng.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName t_order_status
 */
@Data
public class TOrderStatus implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String allStatus;

    private static final long serialVersionUID = 1L;
}