package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * @TableName t_par
 */
@Data
public class TPar implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String tParStatus;

    /**
     * 
     */
    private String tParStatus2;

    /**
     * 
     */
    private String staffName;

    /**
     * 
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date tParTime;

    /**
     * 
     */
    private String tAprDesc;

    /**
     * 申请公司ID
     */
    private String tParComA;

    /**
     * 被申请公司ID
     */
    private String tParComB;

    /**
     * 是客户(0)还是供应商(1)
     */
    private String tParRel;
    /**
     * 返利金额
     */
    private Double rebateAmount;

    /**
     * 返利率
     */
    private Integer rebateRate;

    /**
     * 公司
     */
    @TableField(exist = false)
    private SCompany sCompany;


    public String gettParComB() {
        return tParComB;
    }

    public void settParComB(String tParComB) {
        this.tParComB = tParComB;
    }

    public String gettParRel() {
        return tParRel;
    }

    public void settParRel(String tParRel) {
        this.tParRel = tParRel;
    }

    private static final long serialVersionUID = 1L;
}