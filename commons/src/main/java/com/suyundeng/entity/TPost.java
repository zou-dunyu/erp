package com.suyundeng.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName t_post
 */
@Data
public class TPost implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 职位名称
     */
    private String name;

    /**
     * 所在部门id
     */
    private String dId;

    private static final long serialVersionUID = 1L;
}