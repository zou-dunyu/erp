package com.suyundeng.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName t_printer
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TPrinter implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 编号
     */
    private String pNum;

    /**
     * 服务器
     */
    private String pServer;

    /**
     * 描述
     */
    private String pDescribe;
    /**
     * 可打印型号
     */
    private String pType;

    /**
     * IP地址
     */
    private String pIp;

    /**
     * 责任产线
     */
    private String pLine;

    /**
     * 备注
     */
    private String pRemark;

    /**
     * 公司id
     */
    private String companyId;

    private static final long serialVersionUID = 1L;
}