package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * 
 * @TableName t_product
 */
@Data
@JsonIgnoreProperties(value = "handler")
public class TProduct implements Serializable {
    /**
     * 商品id
     */
    private String id;

    /**
     * 商品编号
     */
    private String pNumber;

    /**
     * 商品名称
     */
    private String pName;

    /**
     * 商品图片
     */
    private String pImg;

    /**
     * 商品价格
     */
    private Double pPrice;

    /**
     * 分类
     */
    private String pEnterprise;

    /**
     * 添加时间
     */
    private Date pAddtime;

    /**
     * 申请状态
     */
    private String aStatus;

    /**
     * 商品状态
     */
    private String pStatus;

    /**
     * 公司id
     */
    private String companyId;

    /**
     * 产地
     */
    private String pLocal;

    /**
     * 保质期
     */
    private String pShelflife;

    /**
     * 材质
     */
    private String pMaterial;

    /**
     * 品牌
     */
    private String pBrand;

    /**
     * 商品质量------这个对应的是商品的库存量
     */
    private Double pQuality;

    /**
     * 商品种类
     */
    private String pKind;

    /**
     * 备注
     */
    private String pRemarks;

    /**
     * 销量
     */
    @TableField(exist = false)
    private Integer pSales;

    /**
     * 商品属性名（比如体积）
     */
    @TableField(exist = false)
    private String attrName;

    /**
     * 商品属性值（比如体积）
     */
    @TableField(exist = false)
    private String attrValue;

    /**
     * 商品分类
     */
    @TableField(exist = false)
    private String categorise;

    /**
     * 商品库存状态
     */
    @TableField(exist = false)
    private String productStatus;

    private static final long serialVersionUID = 1L;
}