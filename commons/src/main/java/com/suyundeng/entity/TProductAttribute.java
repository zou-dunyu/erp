package com.suyundeng.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName t_product_attribute
 */
@Data
public class TProductAttribute implements Serializable {
    /**
     * 商品id
     */
    private String productId;

    /**
     * 商品规格
     */
    private String pNames;

    /**
     * 规格尺寸
     */
    private String pValue;

    private static final long serialVersionUID = 1L;
}