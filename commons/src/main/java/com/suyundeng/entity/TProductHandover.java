package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 
 * @TableName t_product_handover
 */
@Data
public class TProductHandover implements Serializable {
    /**
     * 交接单号
     */
    private String id;

    /**
     * 订单编号
     */
    private String orderId;
    /**
     * 产品名称
     */
    @TableField(exist = false)
    private String pName;
    /**
     * 产品型号
     */
    @TableField(exist = false)
    private String pEnterprise;
    /**
     * 生产部门
     */
    @TableField(exist = false)
    private String productionName;

    /**
     * 仓库名称
     */
    private String wareName;

    /**
     * 入库数量
     */
    private Integer num;

    /**
     * 交接时间
     */
    private Date handoverDate;

    /**
     * 交接人
     */
    private String head;

    /**
     * 备注
     */
    private String remake;

    /**
     * 状态  0未查看 1查看
     */
    private Integer state;

    /**
     * 订单类型 0销售订单 1采购订单
     */
    private Integer type;

    /**
     * 公司id
     */
    private String cid;
    /**
     * 公司名称
     */
    @TableField(exist = false)
    private String companyName;

    private static final long serialVersionUID = 1L;
}