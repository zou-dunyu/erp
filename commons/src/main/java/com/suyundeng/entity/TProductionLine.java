package com.suyundeng.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName t_production_line
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TProductionLine implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * cdf
     */
    private String id;

    private String productionName;

    private String productionCapacity;

    private String productionType;

    private String principal;

    private Integer num;

    private Integer state;

    private String remark;


}