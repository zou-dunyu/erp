package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName t_productive_process
 */
@Data
public class TProductiveProcess implements Serializable {
    /**
     * 订单编号
     */
    private String no;

    /**
     * 时间
     */
    private Date time;

    /**
     * 状态
     */
    private Integer status;

    private static final long serialVersionUID = 1L;
}