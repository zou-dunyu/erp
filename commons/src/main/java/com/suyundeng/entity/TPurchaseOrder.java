package com.suyundeng.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

@Data
public class TPurchaseOrder implements Serializable {

  @TableId(type = IdType.AUTO)
  private String id;
  private String orderId;
  private String productName;
  private long demandQuantity;
  private String paymentMethod;
  private String applicantId;
  private String director;
  private String demandDepartmentId;
  private java.sql.Timestamp demandTime;
  private double estimatedCost;
  private java.sql.Timestamp applicationTime;
  private String applicationType;
  private String description;
  private String applicationStatus;
  private String approver;
  private String approvalDescription;
  private java.sql.Timestamp transactionTime;
  private String transactionNumber;
  private String productNumber;
  private String productColor;
  private java.sql.Timestamp auditTime;
  private String pStatus;

  //公司名称
  @TableField(exist = false)
  private String companyName;

  //公司id
  @TableField(exist = false)
  private String companyId;

  //部门名称
  @TableField(exist = false)
  private String deptName;

  //申请人
  @TableField(exist = false)
  private String applicant;

  //商品类别
  @TableField(exist = false)
  private String categoryName;

  //收货地址
  @TableField(exist = false)
  private String endPlace;

  //买家公司名称
  @TableField(exist = false)
  private String buyer;

  //卖家公司名称
  @TableField(exist = false)
  private String seller;

  //出发地址
  @TableField(exist = false)
  private String startPlace;
}
