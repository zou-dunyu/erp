package com.suyundeng.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 
 * @TableName t_relation
 */
@Data
public class TRelation implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 公司备注
     */
    private String rRemark;

    /**
     * 公司简介
     */
    private String rSyno;

    /**
     * 客户公司id
     */
    private String customerId;

    /**
     * 供应商公司id
     */
    private String supplierId;

    /**
     * 返利金额
     */
    private Double rebateAmount;

    /**
     * 返利率
     */
    private Integer rebateRate;

    /**
     * 合同有无
     */
    private String contract;

    /**
     * 客户公司
     */
    @TableField(exist = false)
    private SCompany sCompany;
    private static final long serialVersionUID = 1L;

    public TRelation() {
    }

    public TRelation(String id, String rRemark, String rSyno, String customerId, String supplierId, Double rebateAmount, Integer rebateRate, String contract) {
        this.id = id;
        this.rRemark = rRemark;
        this.rSyno = rSyno;
        this.customerId = customerId;
        this.supplierId = supplierId;
        this.rebateAmount = rebateAmount;
        this.rebateRate = rebateRate;
        this.contract = contract;
        this.sCompany = sCompany;
    }
}