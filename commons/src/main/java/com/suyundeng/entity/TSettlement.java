package com.suyundeng.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName t_settlement
 */
@Data
public class TSettlement implements Serializable {
    /**
     * 物流结算表id
     */
    private String settlementId;

    /**
     * 运货单号
     */
    private String shippingNum;

    /**
     * 结算状态
     */
    private String settlementStatus;

    /**
     * 结算金额
     */
    private String settlementMoney;

    private static final long serialVersionUID = 1L;
}