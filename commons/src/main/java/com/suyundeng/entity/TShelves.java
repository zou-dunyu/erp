package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 
 * @TableName t_shelves
 */
@Data
public class TShelves implements Serializable {
    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String shelvesNo;

    /**
     * 
     */
    private String wareId;

    /**
     * 
     */
    private String damId;

    /**
     * 
     */
    private String mainProduct;

    /**
     * 
     */
    private Integer totalNum;

    /**
     * 
     */
    private Date creatTime;

    /**
     * 
     */
    private String status;

    /**
     * 
     */
    private String remark;

    /**
     * 
     */
    private String companyId;

    /*
    * 库区名称
    * */
    @TableField(exist = false)
    private String goodsName;

    /*
    * 库区编号
    * */
    @TableField(exist = false)
    private String goodsNo;

    /*
    * 仓库负责人
    *
    * */
    @TableField(exist = false)
    private String person;

    /*
    * 仓库负责人电话
    * */
    @TableField(exist = false)
    private String telephone;

    /*
    * 仓库名
    * */
    @TableField(exist = false)
    private String wareName;
    /*
    * 仓库详情
    * */
    @TableField(exist = false)
    private String twDetails;

    /*
    * 空置货位
    * */
    @TableField(exist = false)
    private String emptySpace;

    private static final long serialVersionUID = 1L;
}