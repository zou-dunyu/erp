package com.suyundeng.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 
 * @TableName t_shipping
 */
@Data
public class TShipping implements Serializable {
    /**
     * 物理id
     */
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 订单编号
     */
    private String orderNum;

    /**
     * 发货单号
     */
    private String shippingNum;

    /**
     * 配送方式id
     */
    private String shippingMethodId;

    /**
     * 配送车辆id
     */
    private String shippingTruckId;

    /**
     * 发货时间
     */
    private String shippingDate;

    /**
     * 司机id
     */
    private String driverId;

    /**
     * 出发地
     */
    private String startPlace;

    /**
     * 目的地
     */
    private String endPlace;

    /**
     * 距离
     */
    private String distance;

    /**
     * 公司id
     */
    private String companyId;

    private static final long serialVersionUID = 1L;
}