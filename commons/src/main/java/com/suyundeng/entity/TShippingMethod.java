package com.suyundeng.entity;


import lombok.Data;

@Data
public class TShippingMethod {

  private String id;
  private String shippingMethodName;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getShippingMethodName() {
    return shippingMethodName;
  }

  public void setShippingMethodName(String shippingMethodName) {
    this.shippingMethodName = shippingMethodName;
  }

}
