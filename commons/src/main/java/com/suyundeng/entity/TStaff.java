package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 
 * @TableName t_staff
 */
@Data
public class TStaff implements Serializable {
    /**
     * 员工id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 登录帐号
     */
    private String loginAccount;

    /**
     * 初始密码
     */
    private String password;

    /**
     * 员工姓名
     */
    private String name;

    /**
     * 员工编号
     */
    private String no;

    /**
     * 联系电话
     */
    private String tel;

    /**
     * 入职时间
     */
    private Date entryTime;

    /**
     * 账号状态
     */
    private String accountStatus;

    /**
     * 员工成绩
     */
    private String grade;

    /**
     * 员工状态
     */
    private String status;

    /**
     * 职位id
     */
    private String pId;

    /**
     * 企业id
     */
    private String cId;

    /**
     * 所属部门id
     */
    private String dId;

    /**
     * 职位名字
     */
    @TableField(exist = false)
    private String pname;

    /**
     *部门名字
     */
    @TableField(exist = false)
    private String dname;

    /**
     *公司名字
     */
    @TableField(exist = false)
    private String cname;

    /**
     * 员工信息集合
     */
    @TableField(exist = false)
    List<TStaff> subStaffs;

    private static final long serialVersionUID = 1L;
}