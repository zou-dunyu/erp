package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 *
 * @TableName t_vehicle
 */
@Data
public class TVehicle implements Serializable {
    /**
     *
     */
    private String vehicleId;

    /**
     *
     */
    private String vehicleName;

    /**
     *
     */
    private String numberPlate;

    /**
     *
     */
    private String vehicleType;

    /**
     *
     */
    private String logisticsId;

    /**
     *
     */
    private String driverId;

    /**
     *
     */
    private String driverCapacity;

    /**
     *
     */
    private String driverSize;

    /**
     *
     */
    private String status;

    /**
     *
     */
    private String remarks;

    /**
     *
     */
    private String createId;

    /**
     *
     */
    private Date createTime;

    /**
     *
     */
    private String updateId;

    /**
     *
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private TLogistics logistics;

    @TableField(exist = false)
    private TDriver driver;

    @TableField(exist = false)
    private SUser user;

    @TableField(exist = false)
    private SUser users;




    @TableField(exist = false)
    private String s_username;

    @TableField(exist = false)
    private String u_username;
}