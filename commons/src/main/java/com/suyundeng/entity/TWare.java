package com.suyundeng.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.ToString;

/**
 * 
 * @TableName t_ware
 */
@Data
@ToString
public class TWare implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 仓库编号
     */
    private String wareNo;

    /**
     * 仓库名称
     */
    private String wareName;

    /**
     * 负责人
     */
    private String person;

    /**
     * 联系电话
     */
    private String telephone;

    /**
     * 货区数目
     */
    private String cargoNum;

    /**
     * 仓库描述
     */
    private String details;

    /**
     * 创建时间
     */
    private Date deteTime;

    /**
     * 状态（0：启用，1：禁用）
     */
    private String status;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 经纬度
     */
    private String place;

    /**
     * 所属公司id
     */
    private String companyId;

    /*
    * 下属库区集合
    * */
    @TableField(exist = false)
    List<TCargoArea> tCargoAreas;
    /*
    * 货架*/
    @TableField(exist = false)
    String totalNum;
    /*
    * 货位*/
    @TableField(exist = false)
    String shelvesNum;
    /*
    空置货位
    * */
    @TableField(exist = false)
    String vacantSpace;
    /*
    * 查询空置货位id*/
    @TableField(exist = false)
    String sId;

    private static final long serialVersionUID = 1L;
}