package com.suyundeng.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 
 * @TableName t_warning
 */
@Data
public class TWarning implements Serializable {
    /**
     * 商品id
     */
    private String productId;

    /**
     * 预警值
     */
    private Integer warningNum;

    /**
     * 仓库id
     */
    private String warehouseId;

    @TableField(exist = false)//忽略该数据，添加数据库是不添加该数据
    private TWare tWare;

    @TableField(exist = false)//忽略该数据，添加数据库是不添加该数据
    private TProduct tProduct;

    private static final long serialVersionUID = 1L;
}