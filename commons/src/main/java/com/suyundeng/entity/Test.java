package com.suyundeng.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName test
 */
@Data
public class Test implements Serializable {
    /**
     * 
     */
    private Integer sid;

    /**
     * 
     */
    private String sname;

    /**
     * 
     */
    private String sclass;

    /**
     * 
     */
    private Integer score;

    private static final long serialVersionUID = 1L;
}