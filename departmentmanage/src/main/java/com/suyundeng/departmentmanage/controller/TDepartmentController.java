package com.suyundeng.departmentmanage.controller;


import com.suyundeng.departmentmanage.service.impl.TDepartmentServiceImpl;
import com.suyundeng.departmentmanage.service.impl.TPostServiceImpl;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TDepartment;
import com.suyundeng.entity.TPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author cps
 * @since 2021-04-19
 */
@RestController
public class TDepartmentController {
@Autowired
private TDepartmentServiceImpl departmentService;
@Autowired
private TPostServiceImpl postService;
@RequestMapping("/department")
public JsonResult getDepartment(){
    List<TDepartment> departments = departmentService.selectDepartment();
    System.out.println(departments);
    return  new JsonResult(1,departments);

}

    @RequestMapping("/selectDepartment")
    public  JsonResult selectDepartment(@RequestParam String name){
        TDepartment tDepartment= departmentService.selectDepartmentAndPost(name);
        System.out.println(tDepartment);
        return  new JsonResult(1,tDepartment);
    }


    @PutMapping("/department")
    public  JsonResult updateDepartment(@RequestBody TDepartment department){
           /* if (Integer.parseInt(department.getPid())==0){

            }*/
       departmentService.updateById(department);

        for (int i = 0; i <department.getPost().size() ; i++) {
            if(postService.getById(department.getPost().get(i).getId())==null){
                department.getPost().get(i).setDId(department.getId());
                postService.save(department.getPost().get(i));
            }
            postService.updateById(department.getPost().get(i));
        }
       return  new JsonResult(1,department);
    }
@PostMapping("/savedepartment")
public JsonResult savaDepartment(@RequestBody  TDepartment department){
    System.out.println(department);
  String did = UUID.randomUUID().toString().replace("-", "").substring(0, 10);
    department.setCId("2");
    department.setId(did);
    String id = department.getId();
    List<TPost> post = department.getPost();
    TPost post1 = post.get(0);
            post1.setDId(id);
    System.out.println(post1);
    System.out.println(department);
    departmentService.save(department);
    postService.save(post1);
    return new JsonResult(1,department);
}

@DeleteMapping("/deletepartment/{id}")
    public JsonResult deleteDepartment(@PathVariable String id){
    TDepartment td1= departmentService.getById(id);
    if (Integer.parseInt(td1.getPid())==0){
        postService.deletePost(id);
        List<TPost> tPosts = postService.selectDid(id);
        for (int i = 0; i <tPosts.size() ; i++) {
            TPost post = tPosts.get(i);
            postService.deletePost(post.getDId());
        }
        departmentService.deleteDepartment(id);
        departmentService.removeById(id);
    }else {
        postService.deletePost(id);
        departmentService.removeById(id);

    }
    return new JsonResult(1,"成功");
}

@DeleteMapping("/deletepost/{id}")
public JsonResult deletePost(@PathVariable String id) {
   postService.removeById(id);
    return new JsonResult(1,"成功");
}

    /**
     * 采购管理-采购申请
     * @return
     */
    @ResponseBody
    @GetMapping("/purchasing/departments")
    public JsonResult getMainDept(){

        try {
            List<TDepartment> data = departmentService.getMainDept();
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }
}
