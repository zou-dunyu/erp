package com.suyundeng.departmentmanage.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.TDepartment;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cps
 * @since 2021-04-19
 */
@Mapper
public interface TDepartmentMapper extends BaseMapper<TDepartment> {
    List<TDepartment> selectDepartment();
    TDepartment selectDepartmentAndPost(String name);
    void deleteDepartment(String id);

    /**
     * 采购管理-采购申请
      * @return
     */
    List<TDepartment> getMainDept();
}
