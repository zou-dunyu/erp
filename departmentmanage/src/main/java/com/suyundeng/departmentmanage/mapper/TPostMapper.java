package com.suyundeng.departmentmanage.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.TPost;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cps
 * @since 2021-04-19
 */
@Mapper
public interface TPostMapper extends BaseMapper<TPost> {
   void deletePost(String did);
  List<TPost>selectDid(String id);

}
