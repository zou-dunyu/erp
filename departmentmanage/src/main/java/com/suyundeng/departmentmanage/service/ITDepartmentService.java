package com.suyundeng.departmentmanage.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TDepartment;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cps
 * @since 2021-04-19
 */
public interface ITDepartmentService extends IService<TDepartment> {
    List<TDepartment> selectDepartment();
    TDepartment selectDepartmentAndPost(String name);
    void deleteDepartment(String id);

    //采购管理-采购申请
    List<TDepartment> getMainDept();
}
