package com.suyundeng.departmentmanage.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TPost;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cps
 * @since 2021-04-19
 */
public interface ITPostService extends IService<TPost> {
    void deletePost(String did);
    List<TPost> selectDid(String id);

}
