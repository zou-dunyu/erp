package com.suyundeng.departmentmanage.service.impl;


import com.suyundeng.departmentmanage.mapper.TDepartmentMapper;
import com.suyundeng.departmentmanage.service.ITDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TDepartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cps
 * @since 2021-04-19
 */
@Service
public class TDepartmentServiceImpl extends ServiceImpl<TDepartmentMapper, TDepartment> implements ITDepartmentService {

    @Autowired
    private TDepartmentMapper departmentMapper;

    @Override
    public List<TDepartment> selectDepartment() {
        return departmentMapper.selectDepartment();
    }

    @Override
    public TDepartment selectDepartmentAndPost(String name) {
        return departmentMapper.selectDepartmentAndPost(name);
    }

    @Override
    public void deleteDepartment(String id) {
        departmentMapper.deleteDepartment(id);
    }

    @Override
    public List<TDepartment> getMainDept() {
        return departmentMapper.getMainDept();
    }
}
