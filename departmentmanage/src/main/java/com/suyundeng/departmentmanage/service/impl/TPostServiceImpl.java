package com.suyundeng.departmentmanage.service.impl;


import com.suyundeng.departmentmanage.mapper.TPostMapper;
import com.suyundeng.departmentmanage.service.ITPostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cps
 * @since 2021-04-19
 */
@Service
public class TPostServiceImpl extends ServiceImpl<TPostMapper, TPost> implements ITPostService {
    @Autowired
    private  TPostMapper postMapper;
    @Override
    public void deletePost(String did) {
        postMapper.deletePost(did);
    }

    @Override
    public List<TPost> selectDid(String id) {
        return postMapper.selectDid(id);
    }




}
