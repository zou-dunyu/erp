package com.suyundeng.logisticmanage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.logisticmanage.entity.JsonResult;
import com.suyundeng.logisticmanage.entity.TDriver;
import com.suyundeng.logisticmanage.service.IDriverService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.SimpleFormatter;


@RestController
public class DriverController {

    @Autowired
    private IDriverService driverService;

    // 分页查询
    @RequestMapping(value = "/pageDrivers",method = RequestMethod.GET)
    public JsonResult pageDriver(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current){
        IPage<TDriver> arg = new Page<>(current, 5);
        IPage<TDriver> page = driverService.pageDriver(arg);
        return new JsonResult(1,page);
    }

    // 添加驾驶员
    @PostMapping("/drivers")
    public JsonResult addDriver(@RequestBody TDriver driver) throws ParseException {
        // 获取创建时间
        Date time = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String t1 = format.format(time);
        Date date = format.parse(t1);
        driver.setCreateTime(date);
        driver.setDriverId(UUID.randomUUID().toString().substring(1,6));
        driverService.save(driver);
        return new JsonResult(1,driver);
    }

    // 删除驾驶员
    @DeleteMapping("/driver/{driverId}")
    public JsonResult deleteDriver(@PathVariable("driverId")String driverId){
        driverService.removeById(driverId);
        return new JsonResult(1,null);
    }

    // 修改驾驶员信息
    @PutMapping("/drivers")
    public JsonResult updateDriver(@RequestBody TDriver driver) throws ParseException {
        // 获取修改时间
        Date time = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String t1 = format.format(time);
        Date date = format.parse(t1);
        driver.setUpdateTime(date);
        driverService.updateById(driver);
        return new JsonResult(1,driver);
    }

    // 搜索
    @PostMapping("/searchDrivers/{name}/{current}")
    public JsonResult searchDrivers(@PathVariable("name") String keyword, @PathVariable("current") Integer current){
        if (keyword != null){
            keyword = "%"+keyword+"%";
        }else {
            keyword = "%%";
        }
        IPage<TDriver> arg = new Page<>(current, 5);
        IPage<TDriver> drivers = driverService.searchDrivers(keyword,arg);
        return new JsonResult(1,drivers);
    }

    @RequestMapping("/selectDrivers/{pageSize}/{keyword}")
    public JsonResult selectDrivers(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current,
                                    @PathVariable("pageSize") Integer pageSize,
                                    @PathVariable("keyword")String keyword){
        keyword = "%"+keyword+"%";
       IPage<TDriver> arg = new Page<>(current, pageSize);
       IPage<TDriver> drivers = driverService.searchDrivers(keyword,arg);
       return new JsonResult(1,drivers);
    }

    @RequestMapping("/selectDrivers/{pageSize}")
    public JsonResult selectDriver(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current,
                                    @PathVariable("pageSize") Integer pageSize){
        String keyword = "%%";
        IPage<TDriver> arg = new Page<>(current, pageSize);
        IPage<TDriver> drivers = driverService.searchDrivers(keyword,arg);
        return new JsonResult(1,drivers);
    }

    @GetMapping("/blb/order/list")
    public JsonResult selectblb(){//李志新---查询所有司机
        List<com.suyundeng.entity.TDriver> tDrivers = driverService.selectOrderList();
        System.out.println(tDrivers);
        return new JsonResult(1,tDrivers);
    }
}
