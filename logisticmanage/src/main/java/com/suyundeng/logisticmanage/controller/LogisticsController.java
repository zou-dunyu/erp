package com.suyundeng.logisticmanage.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.logisticmanage.entity.JsonResult;
import com.suyundeng.logisticmanage.entity.TLogistics;
import com.suyundeng.logisticmanage.service.ILogisticsService;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class LogisticsController {

    @Autowired
    private ILogisticsService logisticsService;

    @RequestMapping(value = "/selectLogistic",method = RequestMethod.GET)
    public JsonResult selectLogistic(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current){
        try {
            IPage<TLogistics> arg = new Page<>(current, 5);
            IPage<TLogistics> logistics = logisticsService.selectLogistic(arg);
            return new JsonResult(1,logistics);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return new JsonResult(0,null);
    }

    @PostMapping("/logisticAdd")
    public JsonResult logisticAdd(@RequestBody TLogistics logistics){
        Date date = new Date();
        logistics.setJoinTime(date);
        logistics.setLogisticsId(UUID.randomUUID().toString().substring(0,12));
        long l = System.currentTimeMillis();
        logistics.setCreateTime(new Date(l));
        logisticsService.save(logistics);

        return new JsonResult(1,null);
    }

    @RequestMapping("/logistics")
    public JsonResult selectLogistics(){
        List<TLogistics> logistics = logisticsService.list();
        return new JsonResult(1,logistics);
    }


    @DeleteMapping("/deleteLogistic/{logisticsId}")
    public void deleteLogistic(@PathVariable String logisticsId){

            logisticsService.removeById(logisticsId);

    }

    @PutMapping("/logisticUpdate")
    public JsonResult logisticUpdate(@RequestBody TLogistics logistic){
        logisticsService.updateById(logistic);
        return new JsonResult(1,null);
    }

    @RequestMapping(value = "/logisticPage/{pageSize}",method = RequestMethod.GET)
    public JsonResult logisticPage(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current,@PathVariable("pageSize") Integer pageSize){
        try {
            IPage<TLogistics> arg = new Page<>(current, pageSize);
            IPage<TLogistics> logistics = logisticsService.selectLogistic(arg);
            return new JsonResult(1,logistics);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return new JsonResult(0,null);
    }

/*
*
* 李志新订单
* */
    @GetMapping("Logistics")
    public com.suyundeng.entity.JsonResult findAll(){
        return new com.suyundeng.entity.JsonResult(1,logisticsService.list());
    }
}
