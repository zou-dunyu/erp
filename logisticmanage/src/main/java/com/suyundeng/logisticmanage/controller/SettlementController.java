package com.suyundeng.logisticmanage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.logisticmanage.entity.JsonResult;
import com.suyundeng.logisticmanage.entity.TSettlement;
import com.suyundeng.logisticmanage.service.ISettlementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SettlementController {

    @Autowired
    private ISettlementService settlementService;

    // 分页查询
    @RequestMapping("/pageSettlement")
    public JsonResult selectAllSettlement(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current){
        IPage<TSettlement> arg = new Page<>(current, 5);
        IPage<TSettlement> page = settlementService.pageSettlement(arg);
        return new JsonResult(1,page);
    }

    // 结算
    @PutMapping("/settlement")
    public JsonResult updateSettlement(@RequestBody TSettlement settlement){
        TSettlement settlement1 = settlementService.getById(settlement.getSettlementId());
        // 需结算的金额
        double newMoney = Double.valueOf(settlement1.getSettlementMoney());
        // 结算的金额
        double oldMoney = Double.valueOf(settlement.getSettlementMoney());
        String money = String.valueOf(newMoney - oldMoney);
        System.out.println("结算之后的金额："+money);
        TSettlement tSettlement = new TSettlement();
        tSettlement.setSettlementId(settlement.getSettlementId());
        tSettlement.setSettlementStatus("1");
        settlementService.updateById(tSettlement);
        return new JsonResult(1,null);
    }

    // 搜索
    @PostMapping("/searchSettlement/{name}/{current}")
    public JsonResult searchSettlement(@PathVariable("name") String keyword, @PathVariable("current") Integer current){
        if (keyword != null){
            keyword = "%"+keyword+"%";
        }else {
            keyword = "%%";
        }
        IPage<TSettlement> arg = new Page<>(current, 5);
        IPage<TSettlement> settlement = settlementService.searchSettlement(keyword,arg);
        return new JsonResult(1,settlement);
    }

    @RequestMapping("/selectSettlements/{pageSize}/{keyword}")
    public JsonResult selectSettlements(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current,
                                        @PathVariable("pageSize") Integer pageSize,
                                        @PathVariable("keyword") String keyword){
        keyword = "%"+keyword+"%";
        IPage<TSettlement> arg = new Page<>(current, pageSize);
        IPage<TSettlement> settlement = settlementService.searchSettlement(keyword,arg);
        return new JsonResult(1,settlement);
    }

    @RequestMapping("/selectSettlements/{pageSize}")
    public JsonResult selectSettlement(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current,
                                        @PathVariable("pageSize") Integer pageSize){
        String keyword = "%%";
        IPage<TSettlement> arg = new Page<>(current, pageSize);
        IPage<TSettlement> settlement = settlementService.searchSettlement(keyword,arg);
        return new JsonResult(1,settlement);
    }

    @RequestMapping("/settlements/{status}/{current}")
    public JsonResult selectStatus(@PathVariable("status")String status, @PathVariable("current") Integer current){
        IPage<TSettlement> arg = new Page<>(current, 5);
        IPage<TSettlement> settlements = settlementService.selectByStatus(status,arg);
        return new JsonResult(1,settlements);
    }
}
