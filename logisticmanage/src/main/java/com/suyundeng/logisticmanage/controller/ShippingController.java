package com.suyundeng.logisticmanage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.suyundeng.entity.TOrder;
import com.suyundeng.entity.TVehicle;
import com.suyundeng.logisticmanage.entity.JsonResult;
import com.suyundeng.logisticmanage.entity.TShipping;
import com.suyundeng.logisticmanage.service.IShippingService;
import com.suyundeng.ordermanage.service.OrderService;
import com.suyundeng.warehousemanage.service.ITVehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class ShippingController {

    @Autowired
    private IShippingService shippingService;

    @Autowired
    private ITVehicleService vehicleService;

    @Autowired
    private OrderService orderService;

    // 结算时改变送货方式
    @PutMapping("/shipping")
    public JsonResult updateShipping(@RequestBody TShipping shipping){
        shippingService.updateById(shipping);
        return new JsonResult(1,null);
    }


    @PostMapping("Shipping")
    public com.suyundeng.entity.JsonResult addShipping(@RequestBody TShipping shipping){//  李志新-订单管理模块添加
        //发货车辆id
        String shippingTruckId = shipping.getShippingTruckId();
        TVehicle vehicle = vehicleService.getOne(new QueryWrapper<TVehicle>().lambda().eq(TVehicle::getVehicleId,shippingTruckId));
        shipping.setDriverId(vehicle.getDriverId());
        shipping.setId(UUID.randomUUID().toString().replace("-",""));
        shipping.setStartPlace("黄冈师范学院");
        shipping.setDistance("10000");
        shippingService.save(shipping);
        TOrder tOrder = new TOrder();
        tOrder.setOrderId(shipping.getOrderNum());
        tOrder.setOrderStatus("3");
        orderService.update(tOrder,new QueryWrapper<TOrder>().lambda().eq(TOrder::getOrderId,shipping.getOrderNum()));
        return new com.suyundeng.entity.JsonResult(1,"ok");
    }
}

