package com.suyundeng.logisticmanage.controller;

import com.suyundeng.logisticmanage.entity.JsonResult;
import com.suyundeng.logisticmanage.entity.TShippingMethod;
import com.suyundeng.logisticmanage.service.IShippingMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ShippingMethodController {
    @Autowired
    private IShippingMethodService shippingMethodService;

    @RequestMapping("/methods")
    public JsonResult selectAllShippingMethods(){
        List<TShippingMethod> methods = shippingMethodService.list();
        return new JsonResult(1,methods);
    }
}
