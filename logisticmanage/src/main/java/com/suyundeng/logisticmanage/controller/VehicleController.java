package com.suyundeng.logisticmanage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.logisticmanage.entity.JsonResult;
import com.suyundeng.logisticmanage.entity.TDriver;
import com.suyundeng.logisticmanage.entity.TLogistics;
import com.suyundeng.logisticmanage.entity.TVehicle;
import com.suyundeng.logisticmanage.service.IDriverService;
import com.suyundeng.logisticmanage.service.ILogisticsService;
import com.suyundeng.logisticmanage.service.IVehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
public class VehicleController {

    @Autowired
    private IVehicleService vehicleService;

    @Autowired
    private ILogisticsService logisticsService;

    @Autowired
    private IDriverService driverService;


    //分页查询
    @RequestMapping("/selectVehicle")
    public JsonResult selectVehicles(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current){
        try {
            IPage<TVehicle> arg = new Page<>(current, 5);
            IPage<TVehicle> Vehicles = vehicleService.selectVehicle(arg);
            return new JsonResult(1,Vehicles);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return new JsonResult(0,null);
    }


    //删除
    @DeleteMapping("deleteVehicle/{vehicleId}")
    public JsonResult deleteVehicle(@PathVariable String vehicleId){
        vehicleService.removeById(vehicleId);
        return new JsonResult(1,null);
    }

    //分页模糊查询
    @RequestMapping("/likeVehicle")
    public JsonResult likeVehicle(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current,String name,@RequestParam(value = "pageSize",required = false,defaultValue = "5")Integer pageSize){
        try {
            if (name!=null){
                name = "%"+name+"%";
            }

            IPage<TVehicle> arg = new Page<>(current, pageSize);
            IPage<TVehicle> Vehicles =  vehicleService.selectVehicleByName(name,arg);
            return new JsonResult(1,Vehicles);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return new JsonResult(0,null);
    }

    //查找公司
    @RequestMapping("addVehicle")
    public JsonResult addVehicle(){
        List<TLogistics> logistics = logisticsService.list();
        return new JsonResult(1,logistics);
    }
//查找司机
    @RequestMapping("selectDriver")
    public JsonResult selectDriver(){
        List<TDriver> drivers = driverService.list();
        return new JsonResult(1,drivers);
    }

    //添加功能
    @PostMapping("vehicleAdd/{logisticsName}/{driverName}")
    public JsonResult vehicleAdd(@RequestBody TVehicle vehicle,@PathVariable("logisticsName") String logisticsName,@PathVariable("driverName") String driverName){
        vehicle.setVehicleId(UUID.randomUUID().toString().substring(0,12));
        TLogistics  logistic = logisticsService.selectLogisticId(logisticsName);
        TDriver driver = driverService.selectDriverId(driverName);

        vehicle.setLogisticsId(logistic.getLogisticsId());
        vehicle.setDriverId(driver.getDriverId());

        vehicleService.save(vehicle);

        return new JsonResult(1,null);
    }

    //更新
    @PutMapping("vehicleUpdate/{logisticsName}/{driverName}")
    public JsonResult vehicleUpdate(@RequestBody TVehicle vehicle,@PathVariable("logisticsName") String logisticsName,@PathVariable("driverName") String driverName){

        TLogistics  logistic = logisticsService.selectLogisticId(logisticsName);
        TDriver driver = driverService.selectDriverId(driverName);

        vehicle.setLogisticsId(logistic.getLogisticsId());
        vehicle.setDriverId(driver.getDriverId());

        vehicleService.updateById(vehicle);
        return new JsonResult(1,null);
    }


    @RequestMapping("/selectVehicles/{pageSize}")
    public JsonResult selectVehicle(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current,@PathVariable("pageSize") Integer pageSize){
        try {
            IPage<TVehicle> arg = new Page<>(current, pageSize);
            IPage<TVehicle> Vehicles = vehicleService.selectVehicle(arg);
            return new JsonResult(1,Vehicles);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return new JsonResult(0,null);
    }

    // 获取所有的车辆信息
    @RequestMapping("/vehicles")
    public JsonResult selectAllVehicles(){
        List<TVehicle> vehicles = vehicleService.list();
        return new JsonResult(1,vehicles);
    }

//    @GetMapping("/vehicle/logisticsID/{id}")
//    public com.suyundeng.entity.JsonResult findByLogisticsId(@PathVariable String id){//车辆信息----李志新
//        List<com.suyundeng.entity.TVehicle> list = vehicleService.list(new QueryWrapper<com.suyundeng.entity.TVehicle>().lambda().eq(com.suyundeng.entity.TVehicle::getLogisticsId, id));
//        return new com.suyundeng.entity.JsonResult(1,list);
//    }

}
