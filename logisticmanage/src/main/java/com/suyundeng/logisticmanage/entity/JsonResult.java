package com.suyundeng.logisticmanage.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class JsonResult {

    private Integer state;

    private Object data;

    public JsonResult(Integer state, Object data) {
        this.state = state;
        this.data = data;
    }

    public JsonResult() {
    }
}
