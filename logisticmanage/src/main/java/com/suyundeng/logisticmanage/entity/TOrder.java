package com.suyundeng.logisticmanage.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName t_order
 */
@Data
public class TOrder implements Serializable {
    /**
     * 订单id
     */
    private String id;

    /**
     * 订单编号
     */
    private String orderId;

    /**
     * 订单类型
     */
    private String orderType;

    /**
     * 产品id
     */
    private String productId;

    /**
     * 付款公司id
     */
    private String customId;

    /**
     * 支付方式
     */
    private String payType;

    /**
     * 交易金额
     */
    private Double money;

    /**
     * 交易时间
     */
    private Date dealTime;

    /**
     * 数量
     */
    private Integer number;

    /**
     * 订单状态 1，2,3,4
     */
    private String orderStatus;

    /**
     * 说明
     */
    private String explains;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 生产计划编号
     */
    private String productPlanNo;

    /**
     * 责任产线id
     */
    private String productLineId;

    /**
     * 计划生产日期
     */
    private Date productPlanTime;

    /**
     * 计划结束生产日期
     */
    private Date productPlanOvertime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 生产状态5,6,7,8,9,10
     */
    private String productStatus;

    /**
     * 打印机
     */
    private String printer;

    /**
     * 生产时间
     */
    private Date productTime;

    /**
     * 激光码
     */
    private String laserCode;

    /**
     * 发货日期
     */
    private Date shippingDate;

    /**
     * 发货方式的id
     */
    private String shippingMethodId;

    /**
     * 接受订单公司id
     */
    private String supplierId;

    /**
     * 下单者
     */
    private String orderHolder;

    /**
     * 供应商公司名称
     */
    @TableField(exist = false)
    private String companyName;

    /**
     * 商品名称
     */
    @TableField(exist = false)
    private String pName;

    /**
     * 商品编号
     */
    @TableField(exist = false)
    private String pNumber;

    /**
     * 申请人
     */
    @TableField(exist = false)
    private String name;

    /**
     * 申请时间
     */
    @TableField(exist = false)
    private String applicationTime;

    /**
     * 需求部门
     */
    @TableField(exist = false)
    private String dName;

    /**
     * 状态名
     */
    @TableField(exist = false)
    private String allStatus;

    private static final long serialVersionUID = 1L;
}