package com.suyundeng.logisticmanage.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName t_settlement
 */
@Data
public class TSettlement implements Serializable {
    /**
     * 物流结算表id
     */
    @TableId
    private String settlementId;

    /**
     * 运货单号
     */
    private String shippingNum;

    /**
     * 结算状态
     */
    private String settlementStatus;

    /**
     * 结算金额
     */
    private String settlementMoney;

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private TShipping shipping;
    @TableField(exist = false)
    private TVehicle vehicle;
    @TableField(exist = false)
    private TLogistics logistics;
    @TableField(exist = false)
    private TOrder order;
    @TableField(exist = false)
    private TProduct product;
    @TableField(exist = false)
    private TProductAttribute productAttribute;

    public String getSettlementId() {
        return settlementId;
    }

    public void setSettlementId(String settlementId) {
        this.settlementId = settlementId;
    }

    public String getShippingNum() {
        return shippingNum;
    }

    public void setShippingNum(String shippingNum) {
        this.shippingNum = shippingNum;
    }

    public String getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(String settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public String getSettlementMoney() {
        return settlementMoney;
    }

    public void setSettlementMoney(String settlementMoney) {
        this.settlementMoney = settlementMoney;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public TShipping getShipping() {
        return shipping;
    }

    public void setShipping(TShipping shipping) {
        this.shipping = shipping;
    }

    public TVehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(TVehicle vehicle) {
        this.vehicle = vehicle;
    }

    public TLogistics getLogistics() {
        return logistics;
    }

    public void setLogistics(TLogistics logistics) {
        this.logistics = logistics;
    }

    public TOrder getOrder() {
        return order;
    }

    public void setOrder(TOrder order) {
        this.order = order;
    }

    public TProduct getProduct() {
        return product;
    }

    public void setProduct(TProduct product) {
        this.product = product;
    }

    public TProductAttribute getProductAttribute() {
        return productAttribute;
    }

    public void setProductAttribute(TProductAttribute productAttribute) {
        this.productAttribute = productAttribute;
    }

    @Override
    public String toString() {
        return "TSettlement{" +
                "settlementId='" + settlementId + '\'' +
                ", shippingNum='" + shippingNum + '\'' +
                ", settlementStatus='" + settlementStatus + '\'' +
                ", settlementMoney='" + settlementMoney + '\'' +
                ", shipping=" + shipping +
                ", vehicle=" + vehicle +
                ", logistics=" + logistics +
                ", order=" + order +
                ", product=" + product +
                ", productAttribute=" + productAttribute +
                '}';
    }
}