package com.suyundeng.logisticmanage.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName t_shipping
 */
@Data
public class TShipping implements Serializable {
    /**
     * 物理id
     */
    private String id;

    /**
     * 订单编号
     */
    private String orderNum;

    /**
     * 发货单号
     */
    private String shippingNum;

    /**
     * 配送方式id
     */
    private String shippingMethodId;

    /**
     * 配送车辆id
     */
    private String shippingTruckId;

    /**
     * 发货时间
     */
    private String shippingDate;

    /**
     * 司机id
     */
    private String driverId;

    /**
     * 出发地
     */
    private String startPlace;

    /**
     * 目的地
     */
    private String endPlace;

    /**
     * 距离
     */
    private String distance;

    /**
     * 公司id
     */
    private String companyId;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getShippingNum() {
        return shippingNum;
    }

    public void setShippingNum(String shippingNum) {
        this.shippingNum = shippingNum;
    }

    public String getShippingMethodId() {
        return shippingMethodId;
    }

    public void setShippingMethodId(String shippingMethodId) {
        this.shippingMethodId = shippingMethodId;
    }

    public String getShippingTruckId() {
        return shippingTruckId;
    }

    public void setShippingTruckId(String shippingTruckId) {
        this.shippingTruckId = shippingTruckId;
    }

    public String getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(String startPlace) {
        this.startPlace = startPlace;
    }

    public String getEndPlace() {
        return endPlace;
    }

    public void setEndPlace(String endPlace) {
        this.endPlace = endPlace;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}