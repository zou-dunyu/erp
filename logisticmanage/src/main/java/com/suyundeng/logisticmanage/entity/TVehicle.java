package com.suyundeng.logisticmanage.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TDriver;
import com.suyundeng.entity.TLogistics;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @TableName t_vehicle
 */
@Data
public class TVehicle implements Serializable {
    /**
     *
     */
    @TableId
    private String vehicleId;

    /**
     *
     */
    private String vehicleName;

    /**
     *
     */
    private String numberPlate;

    /**
     *
     */
    private String vehicleType;

    /**
     *
     */
    private String logisticsId;

    /**
     *
     */
    private String driverId;

    /**
     *
     */
    private String driverCapacity;

    /**
     *
     */
    private String driverSize;

    /**
     *
     */
    private String status;

    /**
     *
     */
    private String remarks;

    /**
     *
     */
    private String createId;

    /**
     *
     */
    private Date createTime;

    /**
     *
     */
    private String updateId;

    /**
     *
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private TLogistics logistics;

    @TableField(exist = false)
    private TDriver driver;

    @TableField(exist = false)
    private SUser user;

    @TableField(exist = false)
    private SUser users;




    @TableField(exist = false)
    private String s_username;

    @TableField(exist = false)
    private String u_username;

}