package com.suyundeng.logisticmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.logisticmanage.entity.TDriver;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DriverMapper extends BaseMapper<TDriver> {
    // 分页查询
    IPage<TDriver> pageDriver(IPage<TDriver> page);

    TDriver selectDriverId(String driverName);

    IPage<TDriver> searchDrivers(@Param("keyword") String keyword, IPage<TDriver> arg);

    List<com.suyundeng.entity.TDriver> selectOrderList(); //李志新
}
