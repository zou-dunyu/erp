package com.suyundeng.logisticmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.logisticmanage.entity.TLogistics;
import org.apache.ibatis.annotations.Mapper;

@Mapper

public interface LogisticsMapper extends BaseMapper<TLogistics> {
    IPage<TLogistics> selectLogistic(IPage<TLogistics> arg);

    TLogistics selectLogisticId(String logisticsName);
}
