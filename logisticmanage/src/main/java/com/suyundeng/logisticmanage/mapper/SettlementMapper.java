package com.suyundeng.logisticmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.logisticmanage.entity.TSettlement;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface SettlementMapper extends BaseMapper<TSettlement> {

    IPage<TSettlement> pageSettlement(IPage<TSettlement> page);

    IPage<TSettlement> searchSettlement(@Param("keyword")String keyword, IPage<TSettlement> arg);

    IPage<TSettlement> selectByStatus(@Param("settlementStatus") String status, IPage<TSettlement> arg);
}
