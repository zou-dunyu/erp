package com.suyundeng.logisticmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.logisticmanage.entity.TShipping;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShippingMapper extends BaseMapper<TShipping> {
}
