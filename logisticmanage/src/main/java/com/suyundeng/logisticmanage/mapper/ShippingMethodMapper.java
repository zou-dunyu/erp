package com.suyundeng.logisticmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.logisticmanage.entity.TShippingMethod;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShippingMethodMapper extends BaseMapper<TShippingMethod> {
}
