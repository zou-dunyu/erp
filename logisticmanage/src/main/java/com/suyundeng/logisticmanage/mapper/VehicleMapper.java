package com.suyundeng.logisticmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.logisticmanage.entity.TVehicle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface VehicleMapper extends BaseMapper<TVehicle> {
    IPage<TVehicle> selectVehicle(IPage<TVehicle> arg);

    IPage<TVehicle> likeVehicle(@Param("types") String types, IPage<TVehicle> arg);

    IPage<TVehicle> selectVehicleByName(@Param("name")String name,IPage<TVehicle> arg);
}
