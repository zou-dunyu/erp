package com.suyundeng.logisticmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.logisticmanage.entity.TDriver;

import java.util.List;

public interface IDriverService extends IService<TDriver> {

    IPage<TDriver> pageDriver(IPage<TDriver> page);

    TDriver selectDriverId(String driverName);

    // 搜索功能
    IPage<TDriver> searchDrivers(String keyword, IPage<TDriver> arg);

    List<com.suyundeng.entity.TDriver> selectOrderList(); //李志新
}
