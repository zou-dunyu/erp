package com.suyundeng.logisticmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.logisticmanage.entity.TLogistics;

public interface ILogisticsService extends IService<TLogistics> {

    IPage<TLogistics> selectLogistic(IPage<TLogistics> arg);

    TLogistics selectLogisticId(String logisticsName);

}
