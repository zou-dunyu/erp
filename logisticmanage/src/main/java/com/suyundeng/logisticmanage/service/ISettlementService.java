package com.suyundeng.logisticmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.logisticmanage.entity.TSettlement;

import java.util.List;

public interface ISettlementService extends IService<TSettlement> {

    IPage<TSettlement> pageSettlement(IPage<TSettlement> page);

    // 模糊查询
    IPage<TSettlement> searchSettlement(String keyword, IPage<TSettlement> arg);

    IPage<TSettlement> selectByStatus(String status, IPage<TSettlement> arg);
}
