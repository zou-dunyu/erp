package com.suyundeng.logisticmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.logisticmanage.entity.TShippingMethod;

public interface IShippingMethodService extends IService<TShippingMethod> {
}
