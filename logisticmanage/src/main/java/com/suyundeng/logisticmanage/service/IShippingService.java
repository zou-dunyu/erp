package com.suyundeng.logisticmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.logisticmanage.entity.TShipping;

public interface IShippingService extends IService<TShipping> {
}
