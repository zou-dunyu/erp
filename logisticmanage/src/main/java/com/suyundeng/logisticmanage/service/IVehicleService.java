package com.suyundeng.logisticmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.logisticmanage.entity.TVehicle;

import java.util.List;

public interface IVehicleService extends IService<TVehicle> {
    IPage<TVehicle> selectVehicle(IPage<TVehicle> arg);


    //IPage<TVehicle> likeVehicle(String types, IPage<TVehicle> arg);

    IPage<TVehicle> selectVehicleByName(String name,IPage<TVehicle> arg);
}
