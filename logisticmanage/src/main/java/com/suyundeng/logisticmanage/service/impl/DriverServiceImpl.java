package com.suyundeng.logisticmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.logisticmanage.entity.TDriver;
import com.suyundeng.logisticmanage.mapper.DriverMapper;
import com.suyundeng.logisticmanage.service.IDriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DriverServiceImpl extends ServiceImpl<DriverMapper, TDriver> implements IDriverService {

    @Autowired
    private DriverMapper driverMapper;

    @Override
    public IPage<TDriver> pageDriver(IPage<TDriver> page) {
        return driverMapper.pageDriver(page);
    }

    @Override
    public TDriver selectDriverId(String driverName) {
        return driverMapper.selectDriverId(driverName);
    }

    @Override
    public IPage<TDriver> searchDrivers(String keyword, IPage<TDriver> arg) {
        return driverMapper.searchDrivers(keyword,arg);
    }

    @Override
    public List<com.suyundeng.entity.TDriver> selectOrderList() {//李志新
        return driverMapper.selectOrderList();
    }
}
