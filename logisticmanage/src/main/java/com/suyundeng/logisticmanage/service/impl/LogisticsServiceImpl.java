package com.suyundeng.logisticmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.logisticmanage.entity.TLogistics;
import com.suyundeng.logisticmanage.mapper.LogisticsMapper;
import com.suyundeng.logisticmanage.service.ILogisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogisticsServiceImpl extends ServiceImpl<LogisticsMapper, TLogistics> implements ILogisticsService {
    @Autowired
    private LogisticsMapper logisticsMapper;
    @Override
    public IPage<TLogistics> selectLogistic(IPage<TLogistics> arg) {
        return logisticsMapper.selectLogistic(arg);
    }

    @Override
    public TLogistics selectLogisticId(String logisticsName) {
        return logisticsMapper.selectLogisticId(logisticsName);
    }


}
