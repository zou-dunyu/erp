package com.suyundeng.logisticmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.logisticmanage.entity.TSettlement;
import com.suyundeng.logisticmanage.mapper.SettlementMapper;
import com.suyundeng.logisticmanage.service.ISettlementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SettlementServiceImpl extends ServiceImpl<SettlementMapper, TSettlement> implements ISettlementService {

    @Autowired
    private SettlementMapper settlementMapper;

    @Override
    public IPage<TSettlement> pageSettlement(IPage<TSettlement> page) {
        return settlementMapper.pageSettlement(page);
    }

    @Override
    public IPage<TSettlement> searchSettlement(String keyword, IPage<TSettlement> arg) {
        return settlementMapper.searchSettlement(keyword,arg);
    }

    @Override
    public IPage<TSettlement> selectByStatus(String status, IPage<TSettlement> arg) {
        return settlementMapper.selectByStatus(status,arg);
    }

}
