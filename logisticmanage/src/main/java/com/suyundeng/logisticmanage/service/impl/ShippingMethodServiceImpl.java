package com.suyundeng.logisticmanage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.logisticmanage.entity.TShippingMethod;
import com.suyundeng.logisticmanage.mapper.ShippingMethodMapper;
import com.suyundeng.logisticmanage.service.IShippingMethodService;
import org.springframework.stereotype.Service;

@Service
public class ShippingMethodServiceImpl extends ServiceImpl<ShippingMethodMapper, TShippingMethod> implements IShippingMethodService {
}
