package com.suyundeng.logisticmanage.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.logisticmanage.entity.TShipping;
import com.suyundeng.logisticmanage.mapper.ShippingMapper;
import com.suyundeng.logisticmanage.service.IShippingService;
import org.springframework.stereotype.Service;

@Service
public class ShippingServiceImpl extends ServiceImpl<ShippingMapper, TShipping> implements IShippingService {
}
