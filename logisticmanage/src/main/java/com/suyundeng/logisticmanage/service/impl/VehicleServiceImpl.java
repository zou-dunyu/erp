package com.suyundeng.logisticmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.logisticmanage.entity.TVehicle;
import com.suyundeng.logisticmanage.mapper.VehicleMapper;
import com.suyundeng.logisticmanage.service.IVehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleServiceImpl extends ServiceImpl<VehicleMapper, TVehicle> implements IVehicleService {

    @Autowired
    private VehicleMapper mapper;
    @Override
    public IPage<TVehicle> selectVehicle(IPage<TVehicle> arg) {
        return mapper.selectVehicle(arg);
    }

    /*@Override
    public IPage<TVehicle> likeVehicle(String types, IPage<TVehicle> arg) {
        return mapper.likeVehicle(types,arg);
    }*/

    @Override
    public IPage<TVehicle> selectVehicleByName(String name,IPage<TVehicle> arg) {
        return mapper.selectVehicleByName(name,arg);
    }
}
