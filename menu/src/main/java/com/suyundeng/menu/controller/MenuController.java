package com.suyundeng.menu.controller;

import com.alibaba.fastjson.JSON;
import com.suyundeng.entity.SMenu;
import com.suyundeng.entity.SUser;
import com.suyundeng.menu.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class MenuController {

    @Autowired
    private IMenuService menuService;



    //根据当前用户获得权限
    @ResponseBody
    @GetMapping("/menus")
    public String getMenus(HttpSession session){
        SUser user = (SUser) session.getAttribute("user");
        List<SMenu> menuList = menuService.selectMenuByUsername(user.getUserName());
        return JSON.toJSONString(menuList);
    }
}
