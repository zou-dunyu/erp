package com.suyundeng.menu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.SMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<SMenu> {

    List<SMenu> selectMenuByUsername(String username);

    List<SMenu> selectMenuSonByPid(String pid);
}
