package com.suyundeng.menu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.SMenu;
import com.suyundeng.entity.SRole;
import com.suyundeng.entity.SUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Mapper
public interface UserMapper extends BaseMapper<SUser> {

    List<SRole> selectRolesByUserName(String username);

    List<SMenu> selectMenusByUsername(String username);

    IPage<SUser> selectPage(Page<SUser> page);
}
