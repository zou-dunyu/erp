package com.suyundeng.menu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.SMenu;

import java.util.List;

public interface IMenuService extends IService<SMenu> {

    List<SMenu> selectMenuByUsername(String username);
}
