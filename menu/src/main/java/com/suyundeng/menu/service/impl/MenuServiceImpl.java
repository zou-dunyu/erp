package com.suyundeng.menu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SMenu;
import com.suyundeng.menu.mapper.MenuMapper;
import com.suyundeng.menu.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, SMenu> implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<SMenu> selectMenuByUsername(String username) {
        return menuMapper.selectMenuByUsername(username);
    }
}
