package com.suyundeng.myclientsmanage.controller;

import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TContract;
import com.suyundeng.myclientsmanage.service.ITContractService;
import com.suyundeng.utils.FtpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DownloadController {

    @Autowired
    private ITContractService itContractService;

    @GetMapping("/download/{id}")
    public JsonResult download(@PathVariable String id) throws Exception {
        TContract contract = itContractService.getById(id);
        String oldName = contract.getFileName().substring(contract.getFileName().lastIndexOf("/")+1);
        String newName = contract.getName()+contract.getFileName().substring(contract.getFileName().lastIndexOf("."));
        FtpUtil.getConnect();
        FtpUtil.download("D://",oldName,newName);
        FtpUtil.close();
        return new JsonResult(1,"成功");
    }
}
