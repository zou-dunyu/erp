package com.suyundeng.myclientsmanage.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TRelation;
import com.suyundeng.myclientsmanage.service.ISCompanyService;
import com.suyundeng.myclientsmanage.service.ITRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@RestController
public class SCompanyController {
    @Autowired
    private ISCompanyService isCompanyService;
    @Autowired
    private ITRelationService itRelationService;

    @PostMapping("/SCompanies/{current}/{page}")
    public JsonResult getSCompanies(@PathVariable Integer current,
                                    @PathVariable Integer page,
                                    String companyName,String productName) {
        Page pg=new Page(current,page);
        IPage<SCompany> sCompanyList = isCompanyService.getSCompanyList(pg, companyName, productName);
        return new JsonResult(1,sCompanyList);
    }

    @GetMapping("/company/{companyId}")
    public JsonResult getCompany(@PathVariable String companyId) {
        return new JsonResult(1,isCompanyService.getOne(new QueryWrapper<SCompany>().lambda().eq(SCompany::getId,companyId)));
    }

    @PostMapping("/SCompany/{remark}")
    public JsonResult getSCompany(@PathVariable String remark, @RequestBody SCompany sCompany, HttpSession session){
        String substring = UUID.randomUUID().toString().replace("-", "").substring(0, 10);
        sCompany.setId(substring);
        sCompany.setCompanyStatus("off");
        String substring1 = UUID.randomUUID().toString().replace("-", "").substring(0, 10);

        SUser user = (SUser) session.getAttribute("user");
        TRelation tRelation = new TRelation(substring1,remark,sCompany.getCompanyIntroduction(),substring,user.getCompanyId(),0.0,0,"0");
        isCompanyService.save(sCompany);
        itRelationService.save(tRelation);
        return new JsonResult(1,"成功！");
    }

//    采购管理-在线下单-供应商列表
    @ResponseBody
    @GetMapping("/purchasing/companies")
    public JsonResult getCompanies(){

        try {

            List<SCompany> data = isCompanyService.list();
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    /**
     * 订单管理需要
     * 修改者：邹敦宇
     * @return
     */
    @GetMapping("/companies")
    public JsonResult findAll(){
        return new JsonResult(1,isCompanyService.list());
    }

}
