package com.suyundeng.myclientsmanage.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TArea;
import com.suyundeng.myclientsmanage.service.ITAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 地区码表 前端控制器
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@RestController
public class TAreaController {
    @Autowired
    private ITAreaService itAreaService;

    //查询省份
    @GetMapping("/Project")
    public JsonResult getProvinces(){
        return new JsonResult(1,itAreaService.list(new QueryWrapper<TArea>().lambda().eq(TArea::getLevel,"1")));
    }

    //查询市和区
    @GetMapping("/child/{parentId}")
    public JsonResult getCities(@PathVariable String parentId){
        return new JsonResult(1,itAreaService.list(new QueryWrapper<TArea>().lambda().eq(TArea::getParentId,parentId)));
    }
}
