package com.suyundeng.myclientsmanage.controller;


import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TContract;
import com.suyundeng.myclientsmanage.service.ITContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wang
 * @since 2021-04-22
 */
@RestController
public class TContractController {
    @Autowired
    private ITContractService itContractService;

    @PostMapping("/TContract")
    public JsonResult saveTContract(@RequestBody TContract tContract, HttpSession session){
        SUser user = (SUser) session.getAttribute("user");
        tContract.setSecond(Integer.parseInt(user.getCompanyId()));//当前员工登录的所属公司id
        tContract.setId(UUID.randomUUID().toString().replace("-","").substring(0,10));
        itContractService.save(tContract);
        return new JsonResult(1,"成功");
    }

    @GetMapping("/contract/{id}")
    public JsonResult getContractById(@PathVariable String id){
        return new JsonResult(1,itContractService.getById(id));
    }

}
