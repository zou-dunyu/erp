package com.suyundeng.myclientsmanage.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.*;
import com.suyundeng.myclientsmanage.service.ITContractService;
import com.suyundeng.myclientsmanage.service.ITParService;
import com.suyundeng.myclientsmanage.service.ITRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wang
 * @since 2021-04-22
 */
@RestController
public class TParController {
    @Autowired
    private ITParService itParService;

    @Autowired
    private ITRelationService itRelationService;

    @Autowired
    private ITContractService itContractService;

    @PostMapping("/TPar")
    public JsonResult saveTpar(@RequestBody TPar tPar, HttpSession session){
        tPar.setId(UUID.randomUUID().toString().replace("-","").substring(0,10));
        tPar.setTParStatus("已申请");
        tPar.setTParStatus2("未查看");
        tPar.setTParTime(new Date());
        SUser user = (SUser) session.getAttribute("user");
        tPar.setTParComA(user.getCompanyId());//根据登录用户查询的公司id
        itParService.save(tPar);
        return new JsonResult(1,"成功！");
    }

    @GetMapping("/TPars/{date1}/{date2}/{current}/{page}")
    public JsonResult getTPars(@PathVariable String date1,
                               @PathVariable String date2,
                               @PathVariable Integer current,
                               @PathVariable Integer page,
                               HttpSession session){
        Page pg=new Page(current,page);
        SUser user = (SUser) session.getAttribute("user");
        IPage<TPar> tParList = itParService.getTParList(pg, date1, date2, user.getCompanyId());
        return new JsonResult(1,tParList);
    }

    @GetMapping("/TPars/{tParComB}")
    public JsonResult getTParByIdb(@PathVariable String tParComB){
        return new JsonResult(1,itParService.list(new QueryWrapper<TPar>().lambda().eq(TPar::gettParComB,tParComB)));
    }
    @PostMapping("/TPars2/{date1}/{date2}/{current}/{page}")
    public JsonResult getTPars2(@PathVariable String date1,
                               @PathVariable String date2,
                               @PathVariable Integer current,
                               @PathVariable Integer page,
                                @RequestBody SCompany sCompany,
                                HttpSession session){
        Page pg=new Page(current,page);
        SUser user = (SUser) session.getAttribute("user");
        IPage<TPar> tParList = itParService.getTParList2(pg,sCompany.getCompanyName(),date1, date2, user.getCompanyId());
        return new JsonResult(1,tParList);
    }

    @GetMapping("/tPar/{id}")
    public JsonResult addTRelationByTParId(@PathVariable String id,HttpSession session){
        TPar tPar = itParService.getById(id);
        TRelation tr=null;
        if (tPar.gettParRel().equals("0")){
            TContract one = itContractService.getOne(new QueryWrapper<TContract>().lambda().eq(TContract::getFirst, tPar.getTParComA()).eq(TContract::getSecond, tPar.gettParComB()));
            if (one==null){
                tr =new TRelation(UUID.randomUUID().toString().replace("-","").substring(0,10),
                        "","",tPar.getTParComA(),tPar.gettParComB(),tPar.getRebateAmount(),tPar.getRebateRate(),"0");
            }else {
                tr =new TRelation(UUID.randomUUID().toString().replace("-","").substring(0,10),
                        "","",tPar.getTParComA(),tPar.gettParComB(),tPar.getRebateAmount(),tPar.getRebateRate(),one.getId());
            }

        }else if (tPar.gettParRel().equals("1")){
            TContract one = itContractService.getOne(new QueryWrapper<TContract>().lambda().eq(TContract::getFirst, tPar.gettParComB()).eq(TContract::getSecond, tPar.getTParComA()));
            if (one==null){
                tr =new TRelation(UUID.randomUUID().toString().replace("-","").substring(0,10),
                        "","",tPar.gettParComB(),tPar.getTParComA(),tPar.getRebateAmount(),tPar.getRebateRate(),"0");
            }else {
                tr =new TRelation(UUID.randomUUID().toString().replace("-","").substring(0,10),
                        "","",tPar.gettParComB(),tPar.getTParComA(),tPar.getRebateAmount(),tPar.getRebateRate(),one.getId());
            }
        }
        SUser user = (SUser) session.getAttribute("user");
        itRelationService.save(tr);
        itParService.update(new UpdateWrapper<TPar>().eq("id",id)
        .set("t_par_status","已同意")
        .set("t_par_status2","通过")
        .set("staff_name",user.getUserName()));
        return new JsonResult(1,"成功");
    }

    @GetMapping("/refuseTPar/{id}/{text}")
    public JsonResult refuse(@PathVariable String id,@PathVariable String text,HttpSession session){
        SUser user = (SUser) session.getAttribute("user");
        itParService.update(new UpdateWrapper<TPar>().eq("id",id)
                .set("t_par_status","已拒绝")
                .set("t_par_status2","拒绝")
                .set("t_apr_desc",text)
                .set("staff_name",user.getUserName()));//root为当前登录的用户名
        return new JsonResult(1,"成功！！");
    }

}
