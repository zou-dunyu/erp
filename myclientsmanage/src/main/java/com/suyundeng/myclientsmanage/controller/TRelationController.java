package com.suyundeng.myclientsmanage.controller;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TRelation;
import com.suyundeng.myclientsmanage.service.ITRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@RestController
public class TRelationController {
    @Autowired
    private ITRelationService itRelationService;

    @PostMapping("/TRelations/{current}/{page}")
    public JsonResult getTRelations(@PathVariable Integer current,
                                    @PathVariable Integer page,
                                    @RequestBody SCompany sCompany,
                                    HttpSession session) {
        SUser user = (SUser) session.getAttribute("user");
        Page pg=new Page(current,page);
        return new JsonResult(1,itRelationService.getTRelationList(pg, user.getCompanyId(), sCompany));
    }

    @PostMapping("/TRelation")
    public JsonResult updateTRelation(@RequestBody TRelation tRelation){
        itRelationService.update(new UpdateWrapper<TRelation>()
                .eq("id",tRelation.getId())
                .set("rebate_amount",tRelation.getRebateAmount())
        .set("rebate_rate",tRelation.getRebateRate()));
        return new JsonResult(1,"成功！！！");
    }

    @GetMapping("/TRelation/{id}")
    public JsonResult getTRelation(@PathVariable String id){
        return new JsonResult(1,itRelationService.getById(id));
    }

}
