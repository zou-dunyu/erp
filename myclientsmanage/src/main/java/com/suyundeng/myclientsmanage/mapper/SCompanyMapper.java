package com.suyundeng.myclientsmanage.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TProduct;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@Mapper
public interface SCompanyMapper extends BaseMapper<SCompany> {
    /**
     * 分页查询
     * @param page
     * @return
     */
    IPage<SCompany> getSCompanyList(Page page, @Param("companyName") String companyName,@Param("productName") String productName);

    /**
     * 采购管理-在线下单-查询商品详情
     * @param productId
     * @return
     */
    String selectCompanyId(String productId);

}
