package com.suyundeng.myclientsmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.TArea;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 地区码表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@Mapper
public interface TAreaMapper extends BaseMapper<TArea> {

}
