package com.suyundeng.myclientsmanage.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.TContract;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2021-04-22
 */
@Mapper
public interface TContractMapper extends BaseMapper<TContract> {

}
