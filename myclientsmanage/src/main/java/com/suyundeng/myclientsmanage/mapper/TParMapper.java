package com.suyundeng.myclientsmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.TPar;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2021-04-22
 */
@Mapper
public interface TParMapper extends BaseMapper<TPar> {
    /**
     * 多表分页模糊查询
     * @param page
     * @param date1
     * @param date2
     * @return
     */
    IPage<TPar> getTParList(Page page, @Param("date1") String date1,@Param("date2") String date2,@Param("id") String id);

    /**
     * 多表分页模糊查询2
     * @param page
     * @param companyName
     * @param date
     * @return
     */
    IPage<TPar> getTParList2(Page page,@Param("companyName") String companyName,@Param("date1") String date1,@Param("date2") String date2,@Param("id") String id);
}
