package com.suyundeng.myclientsmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TRelation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@Mapper
public interface TRelationMapper extends BaseMapper<TRelation> {
    /**
     * 多表分页模糊查询
     */
    IPage<TRelation> getTRelationList(Page page,@Param("supplierId") String supplierId, @Param("Company")SCompany sCompany);
}
