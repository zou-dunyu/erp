package com.suyundeng.myclientsmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.SCompany;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
public interface ISCompanyService extends IService<SCompany> {
    /**
     * 分页查询
     * @param page
     * @param companyName
     * @param productName
     * @return
     */
    IPage<SCompany> getSCompanyList(Page page, String companyName, String productName);

    String selectCompanyId(String productId);
}
