package com.suyundeng.myclientsmanage.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TArea;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 地区码表 服务类
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
public interface ITAreaService extends IService<TArea> {

}
