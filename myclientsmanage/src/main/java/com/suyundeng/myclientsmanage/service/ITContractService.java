package com.suyundeng.myclientsmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TContract;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wang
 * @since 2021-04-22
 */
public interface ITContractService extends IService<TContract> {

}
