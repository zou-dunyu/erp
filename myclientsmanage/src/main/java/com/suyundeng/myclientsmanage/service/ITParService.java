package com.suyundeng.myclientsmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TPar;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wang
 * @since 2021-04-22
 */
public interface ITParService extends IService<TPar> {
    /**
     * 多表分页模糊查询
     * @param page
     * @param date1
     * @param date2
     * @return
     */
    IPage<TPar> getTParList(Page page, String date1, String date2,String id);

    /**
     * 多表分页模糊查询2
     * @param page
     * @param companyName
     * @param date
     * @return
     */
    IPage<TPar> getTParList2(Page page,String companyName,String date1,String date2,String id);
}
