package com.suyundeng.myclientsmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TRelation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
public interface ITRelationService extends IService<TRelation> {
    /**
     * 多表分页模糊查询
     */
    IPage<TRelation> getTRelationList(Page page,String supplierId, @Param("Company") SCompany sCompany);
}
