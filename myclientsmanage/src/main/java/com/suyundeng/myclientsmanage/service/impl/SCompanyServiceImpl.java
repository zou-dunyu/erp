package com.suyundeng.myclientsmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SCompany;
import com.suyundeng.myclientsmanage.mapper.SCompanyMapper;
import com.suyundeng.myclientsmanage.service.ISCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@Service
public class SCompanyServiceImpl extends ServiceImpl<SCompanyMapper, SCompany> implements ISCompanyService {
    @Autowired
    private SCompanyMapper sCompanyMapper;
    @Override
    public IPage<SCompany> getSCompanyList(Page page, String companyName, String productName) {
        return sCompanyMapper.getSCompanyList(page,companyName,productName);
    }

    @Override
    public String selectCompanyId(String productId) {
        return sCompanyMapper.selectCompanyId(productId);
    }
}
