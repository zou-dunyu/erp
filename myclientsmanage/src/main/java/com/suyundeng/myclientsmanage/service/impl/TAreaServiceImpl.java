package com.suyundeng.myclientsmanage.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TArea;
import com.suyundeng.myclientsmanage.mapper.TAreaMapper;
import com.suyundeng.myclientsmanage.mapper.TRelationMapper;
import com.suyundeng.myclientsmanage.service.ITAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地区码表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@Service
public class TAreaServiceImpl extends ServiceImpl<TAreaMapper, TArea> implements ITAreaService {
}
