package com.suyundeng.myclientsmanage.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TContract;
import com.suyundeng.myclientsmanage.mapper.TContractMapper;
import com.suyundeng.myclientsmanage.service.ITContractService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wang
 * @since 2021-04-22
 */
@Service
public class TContractServiceImpl extends ServiceImpl<TContractMapper, TContract> implements ITContractService {

}
