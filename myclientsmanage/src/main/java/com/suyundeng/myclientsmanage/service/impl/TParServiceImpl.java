package com.suyundeng.myclientsmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TPar;
import com.suyundeng.myclientsmanage.mapper.TParMapper;
import com.suyundeng.myclientsmanage.service.ITParService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wang
 * @since 2021-04-22
 */
@Service
public class TParServiceImpl extends ServiceImpl<TParMapper, TPar> implements ITParService {
    @Autowired
    private TParMapper tParMapper;

    @Override
    public IPage<TPar> getTParList(Page page, String date1, String date2,String id) {
        return tParMapper.getTParList(page,date1,date2,id);
    }

    @Override
    public IPage<TPar> getTParList2(Page page, String companyName, String date1,String date2,String id) {
        return tParMapper.getTParList2(page,companyName,date1,date2,id);
    }
}
