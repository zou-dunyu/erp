package com.suyundeng.myclientsmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TRelation;
import com.suyundeng.myclientsmanage.mapper.TRelationMapper;
import com.suyundeng.myclientsmanage.service.ITRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wang
 * @since 2021-04-19
 */
@Service
public class TRelationServiceImpl extends ServiceImpl<TRelationMapper, TRelation> implements ITRelationService {
    @Autowired
    private TRelationMapper tRelationMapper;

    @Override
    public IPage<TRelation> getTRelationList(Page page,String supplierId,SCompany sCompany) {
        return tRelationMapper.getTRelationList(page,supplierId,sCompany);
    }
}
