package com.suyundeng.ordermanage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TOrder;
import com.suyundeng.ordermanage.pojo.OrderOgv;
import com.suyundeng.ordermanage.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class OrderController {

	@Autowired
	private OrderService service;


	@GetMapping("orders/{orderStatus}")
	public JsonResult findAll(@RequestParam(value = "current",required = false,defaultValue="1") Integer current
			,@PathVariable("orderStatus") Integer orderStatus,
			String orderId,String dealTime,HttpSession session){
		Page<TOrder> page = new Page<TOrder>(current, 4);
		if (orderStatus==0){
			orderStatus=null;
		}
		if (orderId==""&&orderId.length()==0 ){
			orderId=null;
		}
		if (dealTime==""&&dealTime.length()==0 ){
			dealTime=null;
		}
		SUser user = (SUser) session.getAttribute("user");
		IPage<TOrder> page1 = service.findPage(page,orderStatus,orderId,dealTime,user.getCompanyId());
		return new JsonResult(1,page1);
	}

	@PutMapping("order")
	public JsonResult updateById(Integer msg,@RequestBody TOrder order){
		if (msg!=1)
			order.setAuditTime(new Date());
		service.updateById(order);
		return new JsonResult(1,"ok");
	}

	@GetMapping("order/id/{orderID}")
	public JsonResult findByOrderID(@PathVariable("orderID") String orderID){
		OrderOgv orderDetails = service.findOrderDetails("", orderID);
		return new JsonResult(1,orderDetails);
	}


	@GetMapping("orders/sort")
	public JsonResult findSortPage(@RequestParam(value = "current",required = false,defaultValue="1") Integer current){
		Page<TOrder> page = new Page<TOrder>(current, 7);
		IPage<TOrder> page1 = service.findSortPage(page,null);
		return new JsonResult(1,page1);
	}

	@PostMapping("order")
	public JsonResult addOrder(@RequestBody TOrder order, HttpSession session){
		SUser user = (SUser) session.getAttribute("user");
		//设置id值
		order.setId(UUID.randomUUID().toString().replace("-",""));
		order.setOrderId(UUID.randomUUID().toString().replace("-","").substring(0,10));
		order.setSupplierId(user.getCompanyId());
		order.setPayType("线下支付");
		order.setOrderStatus("11");
		order.setDealTime(new Date());
		service.save(order);
		return new JsonResult(1,"ok");
	}


	/**
	 * 仓库模块需要
	 * 李志新
	 * @param current
	 * @return
	 */

	@RequestMapping("/selectAllOrder")
	public JsonResult selectVehicles(@RequestParam("current")Integer current){//分页查询
		IPage<TOrder> arg = new Page<>(current, 3);
		IPage<OrderOgv> orders = service.selectVehicle(arg);
		return new JsonResult(1,  orders);
	}
	@PostMapping("/t-order/list/{id}")
	public JsonResult selectList(@PathVariable("id")  String id) {//根据id查询订单
		TOrder tOrder = service.selectListOrderById(id);
		return new JsonResult(1, "ok", tOrder);
	}

 }
