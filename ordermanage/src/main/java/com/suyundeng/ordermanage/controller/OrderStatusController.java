package com.suyundeng.ordermanage.controller;

import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TOrderStatus;
import com.suyundeng.ordermanage.service.OrderStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderStatusController {

	@Autowired
	private OrderStatusService statusService;

	@GetMapping("orderStatus")
	public JsonResult findAll(){
		List<TOrderStatus> list = statusService.list();
		return new JsonResult(1,list);
	}
}
