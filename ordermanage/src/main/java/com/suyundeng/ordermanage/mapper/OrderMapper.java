package com.suyundeng.ordermanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.TOrder;
import com.suyundeng.ordermanage.pojo.OrderOgv;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper extends BaseMapper<TOrder> {
	IPage<TOrder> selectPage(Page<TOrder> page,@Param("orderStatus") Integer orderStatus,
	                         @Param("orderId")String orderId,@Param("dealTime")String dealTime,@Param("supplierId") String supplierId);

	OrderOgv selectOrderDetails(@Param("userID") String userId,String id);

	IPage<TOrder> selectSortPage(Page<TOrder> page,@Param("supplierId")String supplierId);

	IPage<OrderOgv> selectVehicle(IPage<TOrder> arg);//分页查询

	TOrder selectListProduct(@Param("id") String id);//根据id查询订单
}
