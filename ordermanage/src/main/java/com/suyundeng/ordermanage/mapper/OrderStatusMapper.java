package com.suyundeng.ordermanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.TOrderStatus;

public interface OrderStatusMapper extends BaseMapper<TOrderStatus> {
}
