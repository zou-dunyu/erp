package com.suyundeng.ordermanage.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class OrderOgv {
	private String id;
	private String orderType;
	private String allStatus;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date dealTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date shippingDate;
	private Integer number;
	private Double money;
	private String legal;
	private String fixedTel;
	private String endPlace;
	private String logisticsName;
	private String pRemarks;
	private String pName;
	private String pImg;
	private Double pQuality;
	private String pNames;
	private Double pPrice;
	private String pValue;
	private String status;
}
