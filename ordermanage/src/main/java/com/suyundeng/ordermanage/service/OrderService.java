package com.suyundeng.ordermanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TOrder;
import com.suyundeng.ordermanage.pojo.OrderOgv;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderService extends IService<TOrder>  {

	IPage<TOrder> findPage(Page<TOrder> page,Integer orderStatus,String orderId,String dealTime,String supplierId);

	OrderOgv findOrderDetails(String userId, String id);

	IPage<TOrder> findSortPage(Page<TOrder> page,String supplierId);

	IPage<OrderOgv> selectVehicle(IPage<TOrder> arg);//分页查询

	TOrder selectListOrderById(String id);//根据id查询订单
}
