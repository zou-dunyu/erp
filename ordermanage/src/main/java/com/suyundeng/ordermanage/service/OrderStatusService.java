package com.suyundeng.ordermanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TOrderStatus;

public interface OrderStatusService extends IService<TOrderStatus> {
}
