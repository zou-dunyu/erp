package com.suyundeng.ordermanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TOrder;
import com.suyundeng.ordermanage.mapper.OrderMapper;
import com.suyundeng.ordermanage.pojo.OrderOgv;
import com.suyundeng.ordermanage.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, TOrder> implements OrderService {
	@Autowired
	private OrderMapper mapper;

	@Override
	public IPage<TOrder> findPage(Page<TOrder> page,Integer orderStatus,String orderId,String dealTime,String supplierId) {
		return mapper.selectPage(page,orderStatus,orderId,dealTime,supplierId);
	}

	@Override
	public OrderOgv findOrderDetails(String userId, String id) {
		return mapper.selectOrderDetails("1",id);
	}

	@Override
	public IPage<TOrder> findSortPage(Page<TOrder> page, String supplierId) {
		return mapper.selectSortPage(page,"1");
	}

	@Override
	public IPage<OrderOgv> selectVehicle(IPage<TOrder> arg) { //分页查询
		return mapper.selectVehicle(arg);
	}

	@Override
	public TOrder selectListOrderById(String id) {//根据id查询订单
		return mapper.selectListProduct(id);
	}
}
