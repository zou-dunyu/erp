package com.suyundeng.ordermanage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TOrderStatus;
import com.suyundeng.ordermanage.mapper.OrderStatusMapper;
import com.suyundeng.ordermanage.service.OrderStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderStatusServiceImpl extends ServiceImpl<OrderStatusMapper, TOrderStatus> implements OrderStatusService {
	@Autowired
	private OrderStatusMapper mapper;
}
