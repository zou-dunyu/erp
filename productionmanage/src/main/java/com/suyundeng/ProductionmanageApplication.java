package com.suyundeng;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@MapperScan("com.suyundeng.mapper")
@SpringBootApplication
public class ProductionmanageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductionmanageApplication.class, args);
    }

}
