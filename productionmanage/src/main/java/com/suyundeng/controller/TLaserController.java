package com.suyundeng.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TLaser;
import com.suyundeng.service.ITLaserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@RestController
//@RequestMapping("/productionmanage/t-laser")
public class TLaserController {

    @Autowired
    private ITLaserService laserService;

    //分页查询激光码管理
    @RequestMapping(value = "/laserAll",method = RequestMethod.GET)
    private JsonResult getFindAll(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current,
                                  @RequestParam(value = "size",required = false,defaultValue = "10")Integer size){
        Page<TLaser> page = new Page<>(current, size);
        IPage<TLaser> all = laserService.findAll(page);
        return new JsonResult(1, all);
    }

    @RequestMapping("selectByOrderId")
    public JsonResult findByOrderId(String orderId){
        List<TLaser> tLasers = laserService.selectByOrderId(orderId);
        return new JsonResult(1,tLasers);
    }

    //    添加激光码生成列表
    @PostMapping("/addLaser")
    public JsonResult addLaser(String orderId){
        TLaser tLaser = new TLaser();
        tLaser.setId(UUID.randomUUID().toString().substring(0,10));
        tLaser.setLNo(orderId);
        tLaser.setLStatus("-1");
        laserService.save(tLaser);
        return new JsonResult();
    }

    //批量删除激光列表
    @DeleteMapping("/batchDelete")
    public JsonResult queryByDeletion(String[] arr){
        ArrayList<String> strings = new ArrayList<>(Arrays.asList(arr));
        laserService.removeByIds(strings);
        return new JsonResult(1,null);
    }

    @PutMapping("/generation")
    public JsonResult create(String[] arr){
        for (int i = 0; i < arr.length; i++) {
            String lCode = UUID.randomUUID().toString().substring(1,10);
            String lId = arr[i];
            laserService.generationLaser(lId,lCode);
        }
        return new JsonResult(1,null);
    }
}
