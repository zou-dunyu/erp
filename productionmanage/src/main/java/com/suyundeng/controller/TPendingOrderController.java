package com.suyundeng.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TOrder;
import com.suyundeng.entity.TPrinter;
import com.suyundeng.entity.TProductionLine;
import com.suyundeng.service.ITPendingOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@RestController
//@RequestMapping("/productionmanage/t-product-handover")
public class TPendingOrderController {

    @Autowired
    private ITPendingOrderService pendingOrderService;

//    分页查询待生产订单表
    @RequestMapping(value = "/pendingAll",method = RequestMethod.GET)
    public JsonResult getFindAll(@RequestParam(value = "current",required = false,defaultValue = "1")Integer current,
                                  @RequestParam(value = "size",required = false,defaultValue = "10")Integer size){
        Page<TOrder> page = new Page<>(current, size);
        IPage<TOrder> all = pendingOrderService.findAll(page);
        return new JsonResult(1,all);
    }

//    根据订单号和生产状态查询
    @RequestMapping("/pendingByCondition")
    public JsonResult getSelectByOrderIdAndState(String orderId,String state){
        List<TOrder> byOrderIdAndState = pendingOrderService.findByOrderIdAndState(orderId, state);
        if (byOrderIdAndState!=null) {
            return new JsonResult(1, byOrderIdAndState);
        }else {
            return new  JsonResult(0,byOrderIdAndState);
        }
    }

//    查询生产线下拉框
    @RequestMapping("/line")
    public JsonResult getLines(){
        List<TProductionLine> allLine = pendingOrderService.findAllLine();
        return new JsonResult(1,allLine);
    }

//    修改生产产线 打印机 生产时间
    @PutMapping("updateProduct")
    public JsonResult updateProduct(String id ,String productLineId,String productTime,String printer){
        pendingOrderService.modifyProduct(id, productLineId, productTime, printer);
        return new JsonResult(1,null);
    }

//    查询打印机下拉框
    @RequestMapping("printer")
    public JsonResult getPrinter(){
        List<TPrinter> allPrinter = pendingOrderService.findAllPrinter();
        return  new JsonResult(1,allPrinter);
    }


}
