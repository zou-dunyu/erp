package com.suyundeng.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TPrinter;
import com.suyundeng.service.ITPrinterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@RestController
public class TPrinterController {
    @Autowired
    private ITPrinterService printerService;

    /* 打印机分页*/
    @RequestMapping(value = "/printers",method = RequestMethod.GET)
    public JsonResult pagePrinters(@RequestParam(value = "current",required = false,defaultValue = "1")
                                           Integer current){
        IPage<TPrinter> arg = new Page<>(current,5);
        IPage<TPrinter> page = printerService.getTPrintersPage(arg);
        return new JsonResult(1, page);
    }
    /* 打印机添加*/
    @PostMapping("/printer")
    public JsonResult savePrinter(@RequestBody TPrinter printer){
        printer.setId(UUID.randomUUID().toString().substring(0,10));
        printerService.save(printer);
        return new JsonResult(1, printer);
    }

    /* 打印机修改*/
    @PutMapping("/printer")
    public JsonResult updatePrinter(@RequestBody TPrinter printer){
        printerService.updateById(printer);
        return new JsonResult(1, printer);
    }

    /* 打印机删除*/
    @DeleteMapping("/printer/{id}")
    public JsonResult deletePrinter(@PathVariable("id")String id){
        printerService.removeById(id);
        return new JsonResult(1, null);
    }
    /*
    * 按编号查询打印机*/
    @GetMapping("/printers/pnum")
    public JsonResult getByNum(String pnum){
        List<TPrinter> printers = printerService.list(new QueryWrapper<TPrinter>().lambda()
                .eq(TPrinter::getPNum, pnum));
        return new JsonResult(1, printers);
    }


    /* 打印机批量删除*/
    @DeleteMapping("/printers")
    public JsonResult deletePrinter(String[] ids){
        ArrayList<String> idss = (ArrayList<String>) new ArrayList<>(Arrays.asList(ids));
        printerService.removeByIds(idss);
        return new JsonResult(1, null);
    }
}
