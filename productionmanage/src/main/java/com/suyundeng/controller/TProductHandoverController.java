package com.suyundeng.controller;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.*;
import com.suyundeng.service.ITProductHandoverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@RestController
public class TProductHandoverController {
    @Autowired
    private ITProductHandoverService productHandoverService;
    /* 打印机分页*/
    @RequestMapping(value = "/productHandovers",method = RequestMethod.GET)
    public JsonResult pageProductHandovers(@RequestParam(value = "current",required = false,defaultValue = "1")
                                           Integer current,@RequestParam(value = "pageSize",required = false,defaultValue = "5")
            Integer pageSize){
        IPage<TProductHandover> arg = new Page<>(current,pageSize);
        IPage<TProductHandover> page = productHandoverService.getProductHandoversPage(arg);
        return new JsonResult(1, page);
    }
    /*
     * 模糊查询交接单*/
    @GetMapping("/productHandovers/like")
    public JsonResult getByNum(@RequestParam(value = "current",required = false,defaultValue = "1")
                                           Integer current,@RequestParam(value = "pageSize",required = false,defaultValue = "5")
                                           Integer pageSize,String orderId,String handoverDate){
        IPage<TProductHandover> arg = new Page<>(current,pageSize);
        IPage<TProductHandover> productHandovers = productHandoverService.getTProductHandoversByOrderIdAndHandoverDate(arg,orderId,handoverDate);
        return new JsonResult(1, productHandovers);
    }

    /* 交接单批量删除*/
    @DeleteMapping("/productHandovers")
    public JsonResult deletePrinter(String[] ids){
        ArrayList<String> idss = (ArrayList<String>) new ArrayList<>(Arrays.asList(ids));
        productHandoverService.removeByIds(idss);
        return new JsonResult(1, null);
    }

    @GetMapping("/printHandover")
    public JsonResult printHandover(String[] ids, HttpSession session){
        session.setAttribute("ids",ids);
        return new JsonResult(1,null);
    }

    @RequestMapping(value = "/companyProductHandovers",method = RequestMethod.GET)
    public JsonResult pageCompanyProductHandovers(@RequestParam(value = "current",required = false,defaultValue = "1")
                                                   Integer current, @RequestParam(value = "pageSize",required = false,defaultValue = "5")
                                                   Integer pageSize, HttpSession session, Model model){
        IPage<TProductHandover> arg = new Page<>(current,pageSize);
        Object ids = session.getAttribute("ids");
        if(ids instanceof String[]){
            String[] ids1= (String[]) ids;
            IPage<TProductHandover> page = productHandoverService.getCompanyProductHandoversPage(arg,ids1);
            return new JsonResult(1, page);
        }
        return new JsonResult(0,null);
    }

    @RequestMapping("/handoverOrderId")
    public JsonResult getOrders(){
        List<TOrder> orders = productHandoverService.getOrders();
        return new JsonResult(1,orders);
    }
    @RequestMapping("/wares")
    public JsonResult getWares(){
        List<TWare> wares = productHandoverService.getWares();
        return new JsonResult(1,wares);
    }

    @PostMapping("/productHandover")
    public JsonResult saveHandover(@RequestBody TProductHandover handover){
        handover.setId(UUID.randomUUID().toString().substring(0,10));
        handover.setState(0);
        handover.setType(1);
        productHandoverService.save(handover);
        return new JsonResult(1,handover);
    }
}
