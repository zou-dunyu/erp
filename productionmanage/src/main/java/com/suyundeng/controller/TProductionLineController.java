package com.suyundeng.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TProductionLine;
import com.suyundeng.service.ITProductionLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@RestController
public class TProductionLineController {
    @Autowired
    private ITProductionLineService productionLineService;

   /* 生产线分页*/
    @RequestMapping(value = "/productionLines",method = RequestMethod.GET)
    public JsonResult pageProducts(@RequestParam(value = "current",required = false,defaultValue = "1")
                                           Integer current){
            IPage<TProductionLine> arg = new Page<>(current, 5);
            IPage<TProductionLine> page = productionLineService.getProductionLinePage(arg);
            return new JsonResult(1, page);
    }

    /* 生产线添加*/
    @PostMapping("/productionLine")
    public JsonResult saveProduct(@RequestBody TProductionLine productionLine){
            productionLine.setId(UUID.randomUUID().toString().substring(0,10));
            productionLineService.save(productionLine);
            return new JsonResult(1, productionLine);
    }

    /* 生产线修改*/
    @PutMapping("/productionLine")
    public JsonResult updateProduct(@RequestBody TProductionLine productionLine){

            productionLineService.updateById(productionLine);
            return new JsonResult(1, productionLine);
    }

    /* 生产线删除*/
    @DeleteMapping("/productionLine/{id}")
    public JsonResult deleteProduct(@PathVariable("id")String id){
            productionLineService.removeById(id);
            return new JsonResult(1, null);
    }

    /**
     * 订单管理需要
     * 邹敦宇
     * @return
     */
    @GetMapping("/order/productionLines")
    public JsonResult findAll(){
        return new JsonResult(1,productionLineService.list());
    }
}
