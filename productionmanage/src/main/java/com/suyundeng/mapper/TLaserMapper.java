package com.suyundeng.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TLaser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@Mapper
public interface TLaserMapper extends BaseMapper<TLaser> {

    //激光码管理所有数据
    IPage<TLaser> selectAll(IPage<TLaser> page);

    //根据订单查数据
    List<TLaser> selectByOrderId(String orderId);

    Integer createLaser(@Param("orderId") String orderId, @Param("lCode") String lCode);
}
