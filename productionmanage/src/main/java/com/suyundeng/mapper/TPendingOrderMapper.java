package com.suyundeng.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TOrder;
import com.suyundeng.entity.TPrinter;
import com.suyundeng.entity.TProductionLine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@Mapper
public interface TPendingOrderMapper extends BaseMapper<TOrder> {

        //    待生产订单数据分页查询
        IPage<TOrder> selectAll(IPage<TOrder> page);

        //    根据订单编号和生产状态查询
        List<TOrder> selectByOrderIdAndState(@Param("orderId") String orderId,@Param("productStatus") String productStatus);

        //    查询生产产线下拉框
        List<TProductionLine> selectAllLine();

        //    查询打印机下拉框
        List<TPrinter> selectAllPrinter();

        //    修改生产时间 打印机 生产日期
        Integer updateProduct(@Param("id") String id ,@Param("productLineId") String productLineId,@Param("productTime") String productTime,@Param("printer") String printer);

}
