package com.suyundeng.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TPrinter;
import org.apache.ibatis.annotations.Mapper;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@Mapper
public interface TPrinterMapper extends BaseMapper<TPrinter> {
    /*
     * 打印机*/
    IPage<TPrinter> printersPage(IPage<TPrinter> page);

}
