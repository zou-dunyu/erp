package com.suyundeng.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;

import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TOrder;
import com.suyundeng.entity.TProductHandover;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.TWare;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@Mapper
public interface TProductHandoverMapper extends BaseMapper<TProductHandover> {
    /*
     * 产品交接单*/
    IPage<TProductHandover> productHandoversPage(IPage<TProductHandover> page);

    /*
    * 模糊查询交接单*/
    IPage<TProductHandover> selectTProductHandoversByOrderIdAndHandoverDate(IPage<TProductHandover> page, @Param("orderId") String orderId, @Param("handoverDate") String handoverDate);

    /*打印交接单*/
    IPage<TProductHandover> companyProductHandoversPage(IPage<TProductHandover> page,String[] ids);

    List<TOrder> selectOrders();

    List<TWare> selectWares();

}
