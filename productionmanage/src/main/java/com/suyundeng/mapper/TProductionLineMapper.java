package com.suyundeng.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.TProductionLine;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@Mapper
public interface TProductionLineMapper extends BaseMapper<TProductionLine> {
    /*
     * 生产线分页*/
    IPage<TProductionLine> productionLinePage(IPage<TProductionLine> page);

}
