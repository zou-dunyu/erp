package com.suyundeng.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TLaser;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
public interface ITLaserService extends IService<TLaser> {

    //激光码管理所有数据
    IPage<TLaser> findAll(IPage<TLaser> page);

    //根据订单查数据
    List<TLaser> selectByOrderId(String orderId);

    //生成激光码
    Integer generationLaser(String orderId, String lCode);
}
