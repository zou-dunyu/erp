package com.suyundeng.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TOrder;
import com.suyundeng.entity.TPrinter;
import com.suyundeng.entity.TProductionLine;

import java.util.List;

public interface ITPendingOrderService extends IService<TOrder> {

    //    分页查询待生产订单结果
    IPage<TOrder> findAll(IPage<TOrder> page);

//    根据订单号和生产状态查结果
    List<TOrder> findByOrderIdAndState(String orderId, String state);

//    查询生产线下拉框
    List<TProductionLine> findAllLine();

//    查询打印机下拉框
    List<TPrinter> findAllPrinter();

    //    修改生产时间 打印机 生产日期
    Integer modifyProduct(String id ,String productLineId,String productTime,String printer);
}

