package com.suyundeng.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TPrinter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TProductionLine;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
public interface ITPrinterService extends IService<TPrinter> {

    /*
     * 打印机分页*/
    IPage<TPrinter> getTPrintersPage(IPage<TPrinter> page);

}
