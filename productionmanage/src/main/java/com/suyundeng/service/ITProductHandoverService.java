package com.suyundeng.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TOrder;
import com.suyundeng.entity.TProductHandover;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TWare;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
public interface ITProductHandoverService extends IService<TProductHandover> {
    /*
     * 产品交接单*/
    IPage<TProductHandover> getProductHandoversPage(IPage<TProductHandover> page);

    /*
     * 模糊查询交接单*/
    IPage<TProductHandover> getTProductHandoversByOrderIdAndHandoverDate(IPage<TProductHandover> page,String orderId, String handoverDate);
    /*打印交接单*/
    IPage<TProductHandover> getCompanyProductHandoversPage(IPage<TProductHandover> page,String[] ids);

    List<TOrder> getOrders();

    List<TWare> getWares();

}
