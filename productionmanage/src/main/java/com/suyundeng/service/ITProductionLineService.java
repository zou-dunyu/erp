package com.suyundeng.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TProductionLine;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
public interface ITProductionLineService extends IService<TProductionLine> {
    /*
    * 生产线分页*/
    IPage<TProductionLine> getProductionLinePage(IPage<TProductionLine> page);

}
