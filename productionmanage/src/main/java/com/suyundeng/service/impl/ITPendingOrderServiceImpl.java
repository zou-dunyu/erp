package com.suyundeng.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TOrder;
import com.suyundeng.entity.TPrinter;
import com.suyundeng.entity.TProductionLine;
import com.suyundeng.mapper.TPendingOrderMapper;
import com.suyundeng.service.ITPendingOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ITPendingOrderServiceImpl extends ServiceImpl<TPendingOrderMapper,TOrder> implements ITPendingOrderService {

    @Autowired
    private TPendingOrderMapper mapper;

    @Override
    public IPage<TOrder> findAll(IPage<TOrder> page) {
        return mapper.selectAll(page);
    }

    @Override
    public List<TOrder> findByOrderIdAndState(String orderId, String state) {
        return mapper.selectByOrderIdAndState(orderId,state);
    }

    @Override
    public List<TProductionLine> findAllLine() {
        return mapper.selectAllLine();
    }

    @Override
    public List<TPrinter> findAllPrinter() {
        return mapper.selectAllPrinter();
    }

    @Override
    public Integer modifyProduct(String id, String productLineId, String productTime, String printer) {
        return mapper.updateProduct(id,productLineId,productTime,printer);
    }
}

