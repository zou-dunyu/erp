package com.suyundeng.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TLaser;
import com.suyundeng.mapper.TLaserMapper;
import com.suyundeng.service.ITLaserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@Service
public class TLaserServiceImpl extends ServiceImpl<TLaserMapper, TLaser> implements ITLaserService {

    @Autowired
    private TLaserMapper laserMapper;


    @Override
    public IPage<TLaser> findAll(IPage<TLaser> page) {
        return laserMapper.selectAll(page);
    }

    @Override
    public List<TLaser> selectByOrderId(String orderId) {
        return laserMapper.selectByOrderId(orderId);
    }

    @Override
    public Integer generationLaser(String orderId ,String lCode) {
        return laserMapper.createLaser(orderId,lCode);
    }
}
