package com.suyundeng.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TPrinter;
import com.suyundeng.mapper.TPrinterMapper;
import com.suyundeng.service.ITPrinterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@Service
public class TPrinterServiceImpl extends ServiceImpl<TPrinterMapper, TPrinter> implements ITPrinterService {
    @Autowired
    private TPrinterMapper printerMapper;
    /*
    * 打印机分页*/
    @Override
    public IPage<TPrinter> getTPrintersPage(IPage<TPrinter> page) {
        return printerMapper.printersPage(page);
    }
}
