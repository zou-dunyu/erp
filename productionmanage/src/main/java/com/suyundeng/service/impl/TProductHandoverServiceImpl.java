package com.suyundeng.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TOrder;
import com.suyundeng.entity.TProductHandover;
import com.suyundeng.entity.TWare;
import com.suyundeng.mapper.TProductHandoverMapper;
import com.suyundeng.service.ITProductHandoverService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@Service
public class TProductHandoverServiceImpl extends ServiceImpl<TProductHandoverMapper, TProductHandover> implements ITProductHandoverService {

    @Autowired
    private TProductHandoverMapper productHandoverMapper;
    @Override
    public IPage<TProductHandover> getProductHandoversPage(IPage<TProductHandover> page) {
        return productHandoverMapper.productHandoversPage(page);
    }

    @Override
    public IPage<TProductHandover> getTProductHandoversByOrderIdAndHandoverDate(IPage<TProductHandover> page,String orderId, String handoverDate) {
        return productHandoverMapper.selectTProductHandoversByOrderIdAndHandoverDate(page,orderId, handoverDate);
    }

    @Override
    public IPage<TProductHandover> getCompanyProductHandoversPage(IPage<TProductHandover> page, String[] ids) {
        return productHandoverMapper.companyProductHandoversPage(page,ids);
    }

    @Override
    public List<TOrder> getOrders() {
        return productHandoverMapper.selectOrders();
    }

    @Override
    public List<TWare> getWares() {
        return productHandoverMapper.selectWares();
    }

}
