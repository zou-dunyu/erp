package com.suyundeng.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TProductionLine;
import com.suyundeng.mapper.TProductionLineMapper;
import com.suyundeng.service.ITProductionLineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xie
 * @since 2021-04-19
 */
@Service
public class TProductionLineServiceImpl extends ServiceImpl<TProductionLineMapper, TProductionLine> implements ITProductionLineService {

    @Autowired
    private TProductionLineMapper productionLineMapper;
    /*
     * 生产线分页实现*/
    @Override
    public IPage<TProductionLine> getProductionLinePage(IPage<TProductionLine> page) {
        return productionLineMapper.productionLinePage(page);
    }
}
