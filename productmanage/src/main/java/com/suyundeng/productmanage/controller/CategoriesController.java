package com.suyundeng.productmanage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TCategories;
import com.suyundeng.productmanage.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoriesController {

    @Autowired
    private CategoriesService categoriesService;


    //通过商品名称查询所有商品的分类，展示在左边展示栏区域
    @RequestMapping("/categories/name")
    public JsonResult getProduct(){
        List<TCategories> categories = null;
        try {
            TCategories tCategories = new TCategories();
            categories = categoriesService.selectProdcutByTCategoriesName(tCategories.getName());
            System.out.println("categories"+categories);
            return new JsonResult(1,categories);
        }catch (Exception ex){
            ex.printStackTrace();
            return new JsonResult(0,null);
        }
    }

    //查询所有的分类
    @RequestMapping("/categorieslist")
    public JsonResult selectCategories(){
        List<TCategories> list = categoriesService.list();
        return new JsonResult(1,list);
    }

    /**
     * id查询分类
     * @param id
     * @return
     */
    @GetMapping("/categorie/id/{id}")
    public JsonResult getCategorieById(@PathVariable String id ){
        System.out.println(id);

        List<TCategories> list = categoriesService.list(new QueryWrapper<TCategories>().lambda().eq(TCategories::getId, id));
        TCategories tCategorie = list.get(0);
        return new JsonResult(1,tCategorie);
    }
    @GetMapping("/categorie/userId/{id}")
    public JsonResult getCategorieByuserId(@PathVariable String id){
        System.out.println(id);

        SUser user = categoriesService.getCategoriesByUserId(id);
        return new JsonResult(1,user);
    }

    @RequestMapping("/categories")
    public JsonResult addCategories(@RequestBody TCategories categories){
        System.out.println("添加下级分类进来了");
        categoriesService.save(categories);
        return new JsonResult(1,"ok");
    }




}
