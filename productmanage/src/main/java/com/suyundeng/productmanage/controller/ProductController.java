package com.suyundeng.productmanage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TProduct;
import com.suyundeng.productmanage.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.UUID;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    //分页查询商品信息
    @RequestMapping("/products/{current}/{size}")
    public JsonResult pageSelectProduct(@PathVariable("current") Integer current, @PathVariable("size") Integer size){
        Page<TProduct> page = new Page<>(current, size);
        Page<TProduct> pageProduct = productService.page(page);
        return new JsonResult(1,pageProduct);
    }

    //添加商品记录
    @PostMapping("/product")
    public JsonResult addProduct(@RequestBody TProduct tProduct){
        System.out.println("添加商品记录进来了");
        tProduct.setId(UUID.randomUUID().toString().replace("-","").substring(0,10));
        productService.save(tProduct);
        return new JsonResult(1,"ok");
    }

    //修改商品信息
    @PutMapping("/product")
    public JsonResult updateProduct(@RequestBody TProduct tProduct){
        System.out.println("修改商品记录进来了");
        productService.updateById(tProduct);
        return new JsonResult(1,"ok");
    }

    //通过id删除商品记录
    @DeleteMapping("/product/{id}")
    public JsonResult deleteProduct(@PathVariable("id") String id){
        productService.removeById(id);
        return new JsonResult(1,"ok");
    }


    //商品审核，修改商品的状态
    @RequestMapping("/productexamine/{id}/{astatus}")
    public JsonResult  updateProductExamine(@PathVariable("id") String id,
                                            @PathVariable("astatus") String astatus,TProduct product){
        product.setId(id);
        product.setAStatus(astatus);
        productService.updateById(product);
        return new JsonResult(1,"ok");
    }

    //模糊查询
    @RequestMapping("/productsearchlike/{searchvalue}/{current}/{size}")
    public JsonResult searchProduct(@PathVariable("searchvalue") String searchvalue,
                                    @PathVariable("current") Integer current,@PathVariable("size") Integer size){
        Page<TProduct> page = new Page<>(current, size);
        IPage<TProduct> tProductIPage = productService.selectSearchProduct(searchvalue, page);
        return new JsonResult(1,tProductIPage);
    }

    //采购管理 start
    @ResponseBody
    @GetMapping("/purchasing/products")
    public com.suyundeng.entity.JsonResult getPage(
            @RequestParam(value = "current",required = false,defaultValue = "1")Integer current,
            @RequestParam(value = "pageSize",required = false,defaultValue = "10")Integer pageSize,
            @RequestParam("param")String param,
            @RequestParam("companyId")String companyId){
        try {
            Page<TProduct> page = new Page<>(current,pageSize);
            IPage<TProduct> data = productService.getPage(page,param,companyId);
            return new com.suyundeng.entity.JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new com.suyundeng.entity.JsonResult(0,"error",null);
    }

    @ResponseBody
    @GetMapping("/purchasing/product/{id}")
    public com.suyundeng.entity.JsonResult getProductById(@PathVariable("id")String id){

        try {
            TProduct data = productService.getProductById(id);
            return new com.suyundeng.entity.JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }

        return new com.suyundeng.entity.JsonResult(0,"error",null);
    }
    //采购管理 end

/*
* 李志新
* */
    @PostMapping("/t-product/list/{pNumber}")
    public JsonResult selectList(@PathVariable("pNumber")  String pNumber){//根据id查询订单
        TProduct tProduct=productService.selectListProduct(pNumber);
        return new JsonResult(1,"ok",tProduct);
    }

    @GetMapping("findByCIDProduct")
    public JsonResult findByCIDProduct(HttpSession session){
        //SUser user = (SUser) session.getAttribute("user");user.getCompanyId()
        List<TProduct> list = productService.list(new QueryWrapper<TProduct>().lambda().eq(TProduct::getCompanyId, "1"));
        return new JsonResult(1,list);
    }
}
