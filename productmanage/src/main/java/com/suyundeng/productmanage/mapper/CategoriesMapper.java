package com.suyundeng.productmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TCategories;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface CategoriesMapper extends BaseMapper<TCategories> {

    List<TCategories> selectProdcutByTCategoriesName(String name);

    SUser getCategoriesByUserId(String id);
}
