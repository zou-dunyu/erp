package com.suyundeng.productmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.TProduct;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * productMapper
 */
@Mapper
public interface ProductMapper extends BaseMapper<TProduct> {

    //模糊查询
    IPage<TProduct> selectSearchProduct(@Param("searchvalue") String searchvalue, Page page);

    //    采购管理 start
    IPage<TProduct> selectPageForPurchase(Page<TProduct> page,@Param("param") String param,@Param("companyId") String companyId);

    TProduct getProductById(String id);

    Integer getSalesById(String id);
    //    采购管理 end

    TProduct selectListProduct(@Param("pNumber") String pNumber);


}
