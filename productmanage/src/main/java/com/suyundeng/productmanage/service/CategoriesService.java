package com.suyundeng.productmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TCategories;

import java.util.List;

public interface CategoriesService extends IService<TCategories> {

    List<TCategories> selectProdcutByTCategoriesName(String name);

    SUser getCategoriesByUserId(String id);
}
