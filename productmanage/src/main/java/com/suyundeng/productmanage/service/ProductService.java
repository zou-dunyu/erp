package com.suyundeng.productmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TProduct;

import java.util.List;


/**
 * 商品Service接口，服务类
 */
public interface ProductService extends IService<TProduct> {

    //模糊查询
    IPage<TProduct> selectSearchProduct(String searchvalue, Page page);

    // 采购管理 start
    IPage<TProduct> getPage(Page<TProduct> page,String param,String companyId);

    TProduct getProductById(String id);
    //采购管理 end

    TProduct selectListProduct(String pNumber); //查询对应编号商品
}
