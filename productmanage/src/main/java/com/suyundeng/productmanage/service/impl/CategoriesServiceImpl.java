package com.suyundeng.productmanage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TCategories;
import com.suyundeng.productmanage.mapper.CategoriesMapper;
import com.suyundeng.productmanage.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoriesServiceImpl extends ServiceImpl<CategoriesMapper, TCategories> implements CategoriesService {

    @Autowired
    private CategoriesMapper categoriesMapper;

    @Override
    public List<TCategories> selectProdcutByTCategoriesName(String name) {
        System.out.println("查询树进来了");
        return categoriesMapper.selectProdcutByTCategoriesName(name);
    }

    @Override
    public SUser getCategoriesByUserId(String id) {
        return categoriesMapper.getCategoriesByUserId(id);
    }
}
