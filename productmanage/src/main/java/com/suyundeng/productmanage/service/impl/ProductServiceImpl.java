package com.suyundeng.productmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TCategories;
import com.suyundeng.entity.TProduct;
import com.suyundeng.productmanage.mapper.ProductMapper;
import com.suyundeng.productmanage.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 服务实现类
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, TProduct> implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public IPage<TProduct> selectSearchProduct(String searchvalue, Page page) {
        return productMapper.selectSearchProduct(searchvalue,page);
    }

    @Override
    public IPage<TProduct> getPage(Page<TProduct> page,String param,String companyId) {
        return productMapper.selectPageForPurchase(page,param,companyId);
    }

    @Override
    public TProduct getProductById(String id) {
        return productMapper.getProductById(id);
    }

    @Override
    public TProduct selectListProduct(String pNumber) {//通过编号获取对应商品
        return productMapper.selectListProduct(pNumber);
    }
}
