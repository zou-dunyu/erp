package com.suyundeng.purchasingmanage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TPurchaseOrder;
import com.suyundeng.purchasingmanage.service.IPurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class PurchaseOrderController {

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @ResponseBody
    @GetMapping("/purchasing/audit_orders")
    public JsonResult getPage(@RequestParam(value = "current",defaultValue = "1",required = false)Integer current,
                              @RequestParam(value = "pageSize",defaultValue = "10",required = false)Integer pageSize,
                              @RequestParam(value = "param",defaultValue = "",required = false)String param){

        try {
            Page<TPurchaseOrder> page = new Page<>(current,pageSize);
            IPage<TPurchaseOrder> data = purchaseOrderService.selectPage(page,param);
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    @ResponseBody
    @GetMapping("/purchasing/purchase_orders")
    public JsonResult getApplicationPage(@RequestParam(value = "current",defaultValue = "1",required = false)Integer current,
                              @RequestParam(value = "pageSize",defaultValue = "10",required = false)Integer pageSize,
                              @RequestParam(value = "param",defaultValue = "",required = false)String param){

        try {
            Page<TPurchaseOrder> page = new Page<>(current,pageSize);
            IPage<TPurchaseOrder> data = purchaseOrderService.selectApplicationPage(page,param);
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    @ResponseBody
    @PostMapping("/purchasing/purchase_order")
    public JsonResult addOrder(@RequestBody TPurchaseOrder purchaseOrder){

        try {
            //添加采购申请
            //生成采购订单id
            purchaseOrder.setOrderId(UUID.randomUUID().toString().replace("-","").substring(0,20));
            //设置申请时间
            purchaseOrder.setApplicationTime(new Timestamp(new Date().getTime()));
            //设置状态为未审核2
            purchaseOrder.setApplicationStatus("2");
            boolean data = purchaseOrderService.save(purchaseOrder);
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    //修改申请状态
    @ResponseBody
    @PutMapping("/purchasing/purchase_order")
    public JsonResult updateOrder(@RequestBody TPurchaseOrder purchaseOrder){

        try {
            //修改采购申请状态
            //设置审核时间
            purchaseOrder.setAuditTime(new Timestamp(new Date().getTime()));
            boolean data = purchaseOrderService.updateById(purchaseOrder);
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    //采购清单——销售订单
    @ResponseBody
    @PostMapping("/purchasing/purchase_to_order")
    public JsonResult purchaseToOrder(@RequestBody TPurchaseOrder purchaseOrder, HttpSession session){

        try {
            //修改采购订单和添加销售订单
            boolean data = purchaseOrderService.updateAndSave(purchaseOrder,session);
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    //购买
    @ResponseBody
    @PostMapping("/purchasing/add_to_purchase")
    public JsonResult purchaseToOrder(@RequestBody List<TPurchaseOrder> purchaseOrders){
        try {
            boolean data = false;
            //同一个申请订单，同一个id
            String id = UUID.randomUUID().toString().replace("-","").substring(0,20);
            for (TPurchaseOrder purchaseOrder:purchaseOrders) {
                purchaseOrder.setOrderId(id);
                purchaseOrder.setPaymentMethod("线上支付");
                purchaseOrder.setApplicationTime(new Timestamp(new Date().getTime()));
                purchaseOrder.setApplicationType("线上");
                purchaseOrder.setApplicationStatus("5");
                data = purchaseOrderService.save(purchaseOrder);
            }
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    //物流信息
    @ResponseBody
    @GetMapping("/purchasing/shipping")
    public JsonResult getShipping(@RequestParam("companyId")String companyId,
                                  @RequestParam("transactionNumber")String transactionNumber){

        try {
            TPurchaseOrder data = purchaseOrderService.selectShipping(companyId,transactionNumber);
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    //确认签收
    @ResponseBody
    @PostMapping("/purchasing/sign")
    public JsonResult sign(@RequestParam("orderId")String orderId,
                           @RequestParam("productNumber")String productNumber){

        try {
            boolean data = purchaseOrderService.sign(orderId,productNumber);
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }
}
