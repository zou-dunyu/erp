package com.suyundeng.purchasingmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.TPurchaseOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PurchaseOrderMapper extends BaseMapper<TPurchaseOrder> {

    IPage<TPurchaseOrder> getPage(Page<TPurchaseOrder> page,@Param("param") String param);

    IPage<TPurchaseOrder> getApplicationPage(Page<TPurchaseOrder> page,@Param("param") String param);

    TPurchaseOrder selectShipping(@Param("companyId") String companyId,@Param("transactionNumber") String transactionNumber);

    int sign(@Param("orderId") String orderId,@Param("productNumber") String productNumber);
}
