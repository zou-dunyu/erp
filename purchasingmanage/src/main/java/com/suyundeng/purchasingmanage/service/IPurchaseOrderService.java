package com.suyundeng.purchasingmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TPurchaseOrder;

import javax.servlet.http.HttpSession;

public interface IPurchaseOrderService extends IService<TPurchaseOrder> {

    IPage<TPurchaseOrder> selectPage(Page<TPurchaseOrder> page, String param);

    IPage<TPurchaseOrder> selectApplicationPage(Page<TPurchaseOrder> page, String param);

    boolean updateAndSave(TPurchaseOrder purchaseOrder, HttpSession session);

    TPurchaseOrder selectShipping(String companyId, String transactionNumber);

    boolean sign(String orderId,String productNumber);
}
