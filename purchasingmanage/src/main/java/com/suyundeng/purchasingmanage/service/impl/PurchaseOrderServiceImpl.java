package com.suyundeng.purchasingmanage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.SUser;
import com.suyundeng.entity.TOrder;
import com.suyundeng.entity.TProduct;
import com.suyundeng.entity.TPurchaseOrder;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.myclientsmanage.mapper.SCompanyMapper;
import com.suyundeng.ordermanage.mapper.OrderMapper;
import com.suyundeng.productmanage.mapper.ProductMapper;
import com.suyundeng.purchasingmanage.mapper.PurchaseOrderMapper;
import com.suyundeng.purchasingmanage.service.IPurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Service
public class PurchaseOrderServiceImpl extends ServiceImpl<PurchaseOrderMapper, TPurchaseOrder>
        implements IPurchaseOrderService {

    @Autowired
    private PurchaseOrderMapper mapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private SCompanyMapper companyMapper;



    @Override
    public IPage<TPurchaseOrder> selectPage(Page<TPurchaseOrder> page, String param) {
        return mapper.getPage(page,param);
    }

    @Override
    public IPage<TPurchaseOrder> selectApplicationPage(Page<TPurchaseOrder> page, String param) {
        return mapper.getApplicationPage(page,param);
    }

    @Override
    public boolean updateAndSave(TPurchaseOrder purchaseOrder, HttpSession session) {

        try {
            //通过产品编号和产品名查询产品id
            TProduct product = productMapper.selectOne(new QueryWrapper<TProduct>().eq("p_number",purchaseOrder.getProductNumber()));
            String productId = product.getId();

            // 销售订单id、订单编号和交易编号用uuid生成
            long time = new Date().getTime();
            String orderId = UUID.randomUUID().toString().replace("-","").substring(0,20);
            String transactionNumber = UUID.randomUUID().toString().replace("-","").substring(0,20);

            //根据产品id得到公司id
            String companyId = companyMapper.selectCompanyId(productId);
            if (companyId == null){
                companyId = "";
            }

            // 给供应商新添销售订单（假设当前为公司id2）
            SUser user = (SUser) session.getAttribute("user");
            TOrder order = new TOrder(orderId,transactionNumber,purchaseOrder.getPaymentMethod(),productId,
                    user.getCompanyId(),"支付宝",purchaseOrder.getEstimatedCost(),new Timestamp(time), (int) purchaseOrder.getDemandQuantity(),
                    "1",companyId,"慧慧徐测试");
            if (orderMapper.insert(order)>0){
                //更改状态为已下单（未签收）,给采购订单添加交易时间和交易号
                purchaseOrder.setApplicationStatus("4");
                purchaseOrder.setPStatus("-1");
                //给采购订单添加交易时间和交易号
                purchaseOrder.setTransactionTime(new Timestamp(time));
                purchaseOrder.setTransactionNumber(transactionNumber);
                //更改采购订单信息
                if (mapper.updateById(purchaseOrder)>0){
                    return true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public TPurchaseOrder selectShipping(String companyId, String transactionNumber) {
        return mapper.selectShipping(companyId,transactionNumber);
    }

    @Override
    public boolean sign(String orderId, String productNumber) {
        if (mapper.sign(orderId,productNumber)>0){
            return true;
        }
        return false;
    }
}
