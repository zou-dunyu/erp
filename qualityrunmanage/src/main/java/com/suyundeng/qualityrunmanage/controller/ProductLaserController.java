package com.suyundeng.qualityrunmanage.controller;

import com.suyundeng.entity.JsonResult;
import com.suyundeng.qualityrunmanage.entity.ProductLaser;
import com.suyundeng.qualityrunmanage.service.ProductLaserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商品激光码查询
 */
@RestController
public class ProductLaserController {
    @Autowired
    private ProductLaserService productLaserService;

    /**
     * 激光码查询
     * author：杨立虎
     * @param code
     * @return
     */
    @GetMapping("/productlaser/{code}")
    public JsonResult getProductLaser(@PathVariable String code){
        ProductLaser productLaser = productLaserService.getProductLaser(code);
        return new JsonResult(1,productLaser);
    }
}
