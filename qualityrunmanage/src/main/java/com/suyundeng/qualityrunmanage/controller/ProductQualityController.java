package com.suyundeng.qualityrunmanage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.qualityrunmanage.entity.OrderProduct;
import com.suyundeng.qualityrunmanage.entity.ProductProcess;
import com.suyundeng.qualityrunmanage.service.OrderProductService;
import com.suyundeng.qualityrunmanage.service.ProductProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 生产商品质量过程查询
 */
@RestController
public class ProductQualityController {
    @Autowired
    private OrderProductService orderProductService;
    @Autowired
    private ProductProcessService productProcessService;

    /**
     * 通过订单id查询商品订单信息
     * author：杨立虎
     * @param oid
     * @return
     */
    @GetMapping("/orderproducts/{oid}/{current}/{size}")
    public JsonResult getOrderProductByOid(@PathVariable String oid, @PathVariable Integer current, @PathVariable Integer size){
        IPage<OrderProduct> orderProduct = orderProductService.getOrderProduct(oid,new Page<>(current,size));
        return new JsonResult(1,orderProduct);
    }

    /**
     * 通过订单id查询生产过程
     * author：杨立虎
     * @param oid
     * @return
     */
    @GetMapping("/productprcess/{oid}")
    public JsonResult getProductProcess(@PathVariable String oid){
        List<ProductProcess> productProcesses = productProcessService.selectProductProcess(oid);
        return new JsonResult(1,productProcesses);
    }
}
