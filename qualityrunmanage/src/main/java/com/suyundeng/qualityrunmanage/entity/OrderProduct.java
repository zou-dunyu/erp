package com.suyundeng.qualityrunmanage.entity;

import lombok.Data;

import java.util.Date;

@Data
public class OrderProduct {
    private String pName; //产品名称
    private String orderId; //订单编号
    private String allStatus; //订单状态
    private Date dealTime; //交易时间
    private String orderHolder; //操作人
    private String pMaterial; //产品材料
    private String pBrand; //公司
}
