package com.suyundeng.qualityrunmanage.entity;

import lombok.Data;

@Data
public class ProductLaser {
    private String lCode; //激光码
    private String companyName; //公司名称
    private String orderId; //订单号
    private String pNumber; //商品编号
    private String pName; //商品名称
    private String pQuality; //商品质量
}
