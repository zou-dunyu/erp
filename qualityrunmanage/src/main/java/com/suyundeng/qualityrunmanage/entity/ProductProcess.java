package com.suyundeng.qualityrunmanage.entity;

import lombok.Data;

import java.util.Date;

@Data
public class ProductProcess {
    private Date time; //生产时间
    private String allStatus; //订单状态
    private String operator; //订单人
    private String startPlace; //起始位置
    private String endPlace; //结束位置
}
