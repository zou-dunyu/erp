package com.suyundeng.qualityrunmanage.mapper;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.qualityrunmanage.entity.OrderProduct;
import org.apache.ibatis.annotations.Param;


public interface OrderProductMapper {
    IPage<OrderProduct> getOrderProduct(@Param("oid") String oid, Page page);
}
