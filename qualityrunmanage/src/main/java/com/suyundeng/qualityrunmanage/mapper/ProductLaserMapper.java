package com.suyundeng.qualityrunmanage.mapper;

import com.suyundeng.qualityrunmanage.entity.ProductLaser;

public interface ProductLaserMapper{
    ProductLaser getProductLaser(String code);
}
