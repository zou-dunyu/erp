package com.suyundeng.qualityrunmanage.mapper;


import com.suyundeng.qualityrunmanage.entity.ProductProcess;

import java.util.List;

public interface ProductProcessMapper {
    List<ProductProcess> selectProductProcess(String oid);
}
