package com.suyundeng.qualityrunmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.TProduct;

public interface ProductQualityMapper extends BaseMapper<TProduct> {
}