package com.suyundeng.qualityrunmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.qualityrunmanage.entity.OrderProduct;

import java.util.List;

public interface OrderProductService {
    IPage<OrderProduct> getOrderProduct(String oid, Page page);
}
