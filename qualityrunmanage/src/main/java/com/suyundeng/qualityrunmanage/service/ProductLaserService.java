package com.suyundeng.qualityrunmanage.service;

import com.suyundeng.qualityrunmanage.entity.ProductLaser;

public interface ProductLaserService{
    ProductLaser getProductLaser(String code);
}
