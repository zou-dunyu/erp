package com.suyundeng.qualityrunmanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TProductiveProcess;
import com.suyundeng.qualityrunmanage.entity.ProductProcess;

import java.util.List;

public interface ProductProcessService{
    List<ProductProcess> selectProductProcess(String oid);
}
