package com.suyundeng.qualityrunmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TProduct;
import com.suyundeng.entity.TProductiveProcess;

public interface ProductQualityService extends IService<TProduct> {
}
