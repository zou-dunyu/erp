package com.suyundeng.qualityrunmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.qualityrunmanage.entity.OrderProduct;
import com.suyundeng.qualityrunmanage.mapper.OrderProductMapper;
import com.suyundeng.qualityrunmanage.service.OrderProductService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OrderProductServiceImpl implements OrderProductService {
    @Resource
    private OrderProductMapper orderProductMapper;
    @Override
    public IPage<OrderProduct> getOrderProduct(String oid,Page page) {
        return orderProductMapper.getOrderProduct(oid,page);
    }
}
