package com.suyundeng.qualityrunmanage.service.impl;

import com.suyundeng.qualityrunmanage.entity.ProductLaser;
import com.suyundeng.qualityrunmanage.mapper.ProductLaserMapper;
import com.suyundeng.qualityrunmanage.service.ProductLaserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ProductLaserServiceImpl implements ProductLaserService {
    @Resource
    private ProductLaserMapper productLaserMapper;
    @Override
    public ProductLaser getProductLaser(String code) {
        return productLaserMapper.getProductLaser(code);
    }
}
