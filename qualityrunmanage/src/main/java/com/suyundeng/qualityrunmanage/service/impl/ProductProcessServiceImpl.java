package com.suyundeng.qualityrunmanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TProductiveProcess;
import com.suyundeng.qualityrunmanage.entity.ProductProcess;
import com.suyundeng.qualityrunmanage.mapper.ProductProcessMapper;
import com.suyundeng.qualityrunmanage.service.ProductProcessService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProductProcessServiceImpl   implements ProductProcessService {
    @Resource
    private ProductProcessMapper productProcessMapper;
    @Override
    public List<ProductProcess> selectProductProcess(String oid) {
        return productProcessMapper.selectProductProcess(oid);
    }
}
