package com.suyundeng.qualityrunmanage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TProduct;
import com.suyundeng.entity.TProductiveProcess;
import com.suyundeng.qualityrunmanage.mapper.ProductQualityMapper;
import com.suyundeng.qualityrunmanage.service.ProductQualityService;
import org.springframework.stereotype.Service;

@Service
public class ProductQualityServiceImpl extends ServiceImpl<ProductQualityMapper, TProduct> implements ProductQualityService {
}
