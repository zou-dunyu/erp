new Vue({
    el:"#app",
    data(){
        return{
            tableData: null,
            oid:'', //商品号
            dialogTableVisible: false,
            gridData:null, //详细页表格
            current:1,
            total:0,
            size:5,
            tableFlag:'none'
        }
    },
    methods:{
        //分页查询
        loadPage(current){
            this.current=current;
            if(this.oid!==''){
                axios.get("/orderproducts/"+this.oid+"/"+current+"/"+this.size).then(value => {
                    if(value.data.state===1){
                        if(value.data.data.records.length===0){
                            this.$message({
                                message:"没有查询到数据！",
                                type:"warning"
                            })
                        }else {
                            this.tableFlag='inline';
                        }
                        this.tableData=value.data.data.records;
                        this.total=value.data.data.total;
                        this.size=value.data.data.size;
                        this.current=value.data.data.current;
                    }
                })
            }else {
                this.$message.error("请输入订单编码！！")
            }
        },
        //搜索查询
        search(){
            this.loadPage(this.current);
        },
        //下拉框出发事件
        handleCommand(command) {
            if(this.oid!==''){
                this.size=parseInt(command);
                this.loadPage(this.current);
            }else {
                this.$message.error("请输入订单编号！");
            }
        },
        //会话框弹出
        handleEdit(index, row) {
            this.dialogTableVisible=true;
            axios.get("/productprcess/"+row.orderId).then(value => {
                if(value.data.state===1){
                    this.gridData=value.data.data;
                }else {
                    this.$message({
                        message:"无数据",
                        type:'warning'
                    });
                }
            })
        },
        //表格列样式函数
        cellStyle(row,column,rowIndex,columnIndex) {//根据订单状态显示颜色
            //console.log(row);
            //console.log(row.column);
            if (row.column.label === "订单状态" && row.row.allStatus === "已支付") {
                return 'background:yellow'
            } else if (row.column.label === "订单状态" && row.row.allStatus === "待发货") {
                return 'background:#05F8CF'
            } else if (row.column.label === "订单状态" && row.row.allStatus === "已完成") {
                return 'background:#00A341'
            } else if (row.column.label === "订单状态" && row.row.allStatus === "已发货") {
                return 'background:#05F8CF'
            }
        }
    }
});