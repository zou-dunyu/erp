package com.suyundeng.gao.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.*;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TDepartment;
import com.suyundeng.entity.TStaff;
import com.suyundeng.gao.service.IStaffMapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class StaffManageController {

    @Autowired
    private IStaffMapperService iStaffMapper;

    @RequestMapping("/page/departments")
    public List<TDepartment> getDepartmentNamesAndDepartmentIds(){
        List<TDepartment> departments = iStaffMapper.findDepartmentNamesAndDepartmentIds();
        return departments;
    }

    @RequestMapping("/page/positions")
    public List<TPost> getPositionNamesAndPositionIds(){
        List<TPost> posts = iStaffMapper.findPositionNamesAndPositionIds();
        return posts;
    }

    @RequestMapping("/page/companys")
    public List<SCompany> getCompanyNamesAndCompanyIds(){
        List<SCompany> companys = iStaffMapper.findCompanyNamesAndCompanyIds();
        return companys;
    }

    @RequestMapping("/page/add")
    public void addStaff(@RequestBody TStaff subStaffs){
        iStaffMapper.addStaff(subStaffs);
    }

    @DeleteMapping("/page/{id}")
    public void deleteStaff(@PathVariable Integer id){
        iStaffMapper.modifyStaff(id);
    }

    @RequestMapping("/page/change")
    public void updateStaff(@RequestBody TStaff subStaffs){
        iStaffMapper.modifyStaff(subStaffs);
    }

    @RequestMapping("/page/find/{sname}/{dname}")
    public List<TStaff> getStaffByUsernameAndDepartmentname(@PathVariable String sname, @PathVariable String dname){
        List<TStaff> staff = iStaffMapper.findStaffByUsernameAndDepartmentname(sname, dname);
        return staff;
    }

    /**
     * 采购管理-采购申请
     * @return
     */
    @GetMapping("/purchasing/staffs")
    public JsonResult getStaff(){

        try {
            List<TStaff> data = iStaffMapper.list();
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    @RequestMapping(value = "/staffs",method = RequestMethod.GET)
    public IPage<TStaff> pageStaffs(@RequestParam(value = "current",required = false) Integer current){
        Page<TStaff> arg = new Page<>(current, 2);
        IPage<TStaff> page = iStaffMapper.pageStaffs(arg);
        return page;
    }
}
