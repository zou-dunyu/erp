package com.suyundeng.gao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TDepartment;
import com.suyundeng.entity.TPost;
import com.suyundeng.entity.TStaff;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StaffMapper extends BaseMapper<TStaff> {

    List<TDepartment> selectDepartmentNamesAndDepartmentIds();

    List<TPost> selectPositionNamesAndPositionIds();

    List<SCompany> selectCompanyNamesAndCompanyIds();

    List<TStaff> selectStaffByUsernameAndDepartmentname(@Param("sname") String sname, @Param("dname") String dname);

    void insertStaff(TStaff staff);

    void updateStaff(TStaff staff);

    void deleteStaff(Integer id);

    IPage<TStaff> pageStaffs(IPage<TStaff> page);


}
