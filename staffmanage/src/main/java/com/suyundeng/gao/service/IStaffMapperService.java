package com.suyundeng.gao.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TDepartment;
import com.suyundeng.entity.TPost;
import com.suyundeng.entity.TStaff;

import java.util.List;

public interface IStaffMapperService extends IService<TStaff> {

    List<TDepartment> findDepartmentNamesAndDepartmentIds();

    List<TPost> findPositionNamesAndPositionIds();

    List<SCompany> findCompanyNamesAndCompanyIds();

    List<TStaff> findStaffByUsernameAndDepartmentname(String sname,String dname);

    void addStaff(TStaff staff);

    void modifyStaff(TStaff staff);

    void modifyStaff(Integer id);

    IPage<TStaff> pageStaffs(IPage<TStaff> page);


}
