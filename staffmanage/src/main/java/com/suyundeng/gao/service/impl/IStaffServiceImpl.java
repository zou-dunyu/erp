package com.suyundeng.gao.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SCompany;
import com.suyundeng.entity.TDepartment;
import com.suyundeng.entity.TPost;
import com.suyundeng.entity.TStaff;
import com.suyundeng.gao.mapper.StaffMapper;
import com.suyundeng.gao.service.IStaffMapperService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class IStaffServiceImpl extends ServiceImpl<StaffMapper, TStaff> implements IStaffMapperService {

    @Resource
    private StaffMapper staffMapper;


    @Override
    public List<TDepartment> findDepartmentNamesAndDepartmentIds() {
        return staffMapper.selectDepartmentNamesAndDepartmentIds();
    }

    @Override
    public List<TPost> findPositionNamesAndPositionIds() {
        return staffMapper.selectPositionNamesAndPositionIds();
    }

    @Override
    public List<SCompany> findCompanyNamesAndCompanyIds() {
        return staffMapper.selectCompanyNamesAndCompanyIds();
    }

    @Override
    public List<TStaff> findStaffByUsernameAndDepartmentname(String sname, String dname) {
        return staffMapper.selectStaffByUsernameAndDepartmentname(sname,dname);
    }

    @Override
    public void addStaff(TStaff staff) {
        staffMapper.insertStaff(staff);
    }

    @Override
    public void modifyStaff(TStaff staff) {
        staffMapper.updateStaff(staff);
    }

    @Override
    public void modifyStaff(Integer id) {
        staffMapper.deleteStaff(id);
    }

    @Override
    public IPage<TStaff> pageStaffs(IPage<TStaff> page) {
        return staffMapper.pageStaffs(page);
    }
}
