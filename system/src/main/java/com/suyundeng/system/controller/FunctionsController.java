package com.suyundeng.system.controller;

import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.SMenu;
import com.suyundeng.entity.SUser;
import com.suyundeng.system.service.IFunctionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class FunctionsController {

    @Autowired
    private IFunctionsService functionsService;



    //根据当前用户获得权限
    @ResponseBody
    @GetMapping("/functions")
    public JsonResult getFunctions(HttpSession session){
//        User user = null;
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        Object principal = authentication.getPrincipal();
//        if (principal instanceof User){
//            user = (User) principal;
//        }
        try{
            List<SMenu> functionsList = functionsService.selectDistinctFunByUsername();
            return new JsonResult(1,"ok",functionsList);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    //根据角色id获得权限
    @ResponseBody
    @GetMapping("/function")
    public JsonResult getFunByRoleId(@RequestParam("roleId") String roleId){
        try{
            List<SMenu> data = functionsService.selectFunByRoleId(roleId);
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

}
