package com.suyundeng.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.SUserRole;
import com.suyundeng.system.service.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserRoleController {

    @Autowired
    private IUserRoleService userRoleService;

    @ResponseBody
    @PostMapping("/userRole")
    public JsonResult addRoleByUserId(@RequestParam("roleId")String roleId, @RequestParam("userId")String userId){

        try{
            boolean data = userRoleService.save(new SUserRole(userId,roleId));
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }

    @ResponseBody
    @DeleteMapping("/userRole")
    public JsonResult deleteRoleByUserId(@RequestParam("roleId")String roleId, @RequestParam("userId")String userId){

        try{
            boolean data = userRoleService.remove(
                    new QueryWrapper<SUserRole>().eq("role_id",roleId).eq("user_id",userId));
            return new JsonResult(1,"ok",data);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new JsonResult(0,"error",null);
    }
}
