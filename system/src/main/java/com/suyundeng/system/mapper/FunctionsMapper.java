package com.suyundeng.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.SMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Mapper
public interface FunctionsMapper extends BaseMapper<SMenu> {

    List<SMenu> selectDistinctFunByUsername();

    List<SMenu> selectFunByPid(String pid);

    List<SMenu> selectFunByRoleId(String roleId);
}
