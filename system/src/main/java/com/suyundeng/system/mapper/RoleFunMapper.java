package com.suyundeng.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.SRoleMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Mapper
public interface RoleFunMapper extends BaseMapper<SRoleMenu> {

    int addFunByRoleId(@Param("roleId") String roleId, String[] funIdArr);


    int deleteFunByRoleIdAndFunId(@Param("roleId") String roleId, String[] funIdArr);
}
