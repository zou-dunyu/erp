package com.suyundeng.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.SRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Mapper
public interface RoleMapper extends BaseMapper<SRole> {

    IPage<SRole> selectPage(Page<SRole> page);

    List<SRole> selectRolesByUserId(String userId);

}
