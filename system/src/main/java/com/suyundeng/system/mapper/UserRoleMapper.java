package com.suyundeng.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.SUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<SUserRole> {

}
