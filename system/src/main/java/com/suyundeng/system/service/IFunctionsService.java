package com.suyundeng.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.SMenu;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
public interface IFunctionsService extends IService<SMenu> {

    List<SMenu> selectDistinctFunByUsername();

    List<SMenu> selectFunByRoleId(String roleId);
}
