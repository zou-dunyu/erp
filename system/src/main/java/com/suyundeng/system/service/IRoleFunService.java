package com.suyundeng.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.SRoleMenu;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
public interface IRoleFunService extends IService<SRoleMenu> {

    int addFunByRoleId(String roleId, String[] funIdArr);

    int deleteFunByRoleIdAndFunId(String roleId, String[] funIdArr);

}
