package com.suyundeng.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.SUserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
public interface IUserRoleService extends IService<SUserRole> {

}
