package com.suyundeng.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.SUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
public interface IUserService extends IService<SUser> {

    IPage<SUser> getPage(Page<SUser> page);
}
