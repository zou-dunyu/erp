package com.suyundeng.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SMenu;
import com.suyundeng.system.mapper.FunctionsMapper;
import com.suyundeng.system.service.IFunctionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Service
public class FunctionsServiceImpl extends ServiceImpl<FunctionsMapper, SMenu> implements IFunctionsService {

    @Autowired
    private FunctionsMapper functionsMapper;

    @Override
    public List<SMenu> selectDistinctFunByUsername() {
        return functionsMapper.selectDistinctFunByUsername();
    }

    @Override
    public List<SMenu> selectFunByRoleId(String roleId) {
        return functionsMapper.selectFunByRoleId(roleId);
    }
}
