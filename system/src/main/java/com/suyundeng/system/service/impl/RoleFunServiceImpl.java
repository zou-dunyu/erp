package com.suyundeng.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SRoleMenu;
import com.suyundeng.system.mapper.RoleFunMapper;
import com.suyundeng.system.service.IRoleFunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Service
public class RoleFunServiceImpl extends ServiceImpl<RoleFunMapper, SRoleMenu> implements IRoleFunService {

    @Autowired
    private RoleFunMapper roleFunMapper;

    @Override
    public int addFunByRoleId(String roleId, String[] funIdArr) {
        return roleFunMapper.addFunByRoleId(roleId,funIdArr);
    }

    @Override
    public int deleteFunByRoleIdAndFunId(String roleId, String[] funIdArr) {
        return roleFunMapper.deleteFunByRoleIdAndFunId(roleId,funIdArr);
    }

}
