package com.suyundeng.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SRole;
import com.suyundeng.system.mapper.RoleMapper;
import com.suyundeng.system.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, SRole> implements IRoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public IPage<SRole> getPage(Page<SRole> page) {
        return roleMapper.selectPage(page);
    }

    @Override
    public List<SRole> selectRolesByUserId(String userId) {
        return roleMapper.selectRolesByUserId(userId);
    }
}
