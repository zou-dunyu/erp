package com.suyundeng.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SUserRole;
import com.suyundeng.system.mapper.UserRoleMapper;
import com.suyundeng.system.service.IUserRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, SUserRole> implements IUserRoleService {

}
