package com.suyundeng.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.SUser;
import com.suyundeng.menu.mapper.UserMapper;
import com.suyundeng.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhx
 * @since 2021-04-12
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, SUser> implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public IPage<SUser> getPage(Page<SUser> page) {
        return userMapper.selectPage(page);
    }
}
