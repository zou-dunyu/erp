package com.suyundeng.warehousemanage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TCargoArea;
import com.suyundeng.warehousemanage.service.ITCargoAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class TCargoAreaController {

    @Autowired
    private ITCargoAreaService itCargoAreaService;

    @GetMapping("/t-cargo-area")
    public JsonResult selectAllCargoArea(@RequestParam(value = "current", required = false, defaultValue = "1") Integer current,
                                         @RequestParam("cargoAreaName") String cargoAreaName, @RequestParam("wareId") String wareId
            , @RequestParam("settledDate") String settledDate) throws ParseException {//分页查询库区表
        TCargoArea tCargoArea = new TCargoArea(cargoAreaName, wareId, settledDate);
        IPage<TCargoArea> page = new Page<>(current, 3);
        IPage<TCargoArea> cargoAreaData = itCargoAreaService.selectAllCargoArea(page, tCargoArea);
        return new JsonResult(1, "ok", cargoAreaData);
    }

    @RequestMapping("/list/t-cargo-area")
    public JsonResult area() {                   //查询库区表所有name
        List<TCargoArea> cargoAreaData = itCargoAreaService.list();
        return new JsonResult(1, "ok", cargoAreaData);
    }

    @PostMapping("/addCargoArea")
    public JsonResult addCargoArea(@RequestBody TCargoArea tCargoArea) {//添加数据
        tCargoArea.setId(UUID.randomUUID().toString().replace("-", "").substring(11));
        tCargoArea.setStatus("0");
        LocalDateTime now = LocalDateTime.now();
        tCargoArea.setDeteTime1(Date.from(now.atZone(ZoneId.systemDefault()).toInstant()));//获取当前时间
        itCargoAreaService.save(tCargoArea);
        return new JsonResult(1, "ok", tCargoArea);
    }

    @PutMapping("/t-cargo-area/status")
    public JsonResult updateStatus(@RequestBody TCargoArea tCargoArea) {//修改状态
        if (tCargoArea.getStatus().equals("0")) {
            tCargoArea.setStatus("1");
        } else {
            tCargoArea.setStatus("0");
        }
        itCargoAreaService.updateById(tCargoArea);
        return new JsonResult(1, "ok", tCargoArea);
    }

    @PutMapping("/t-cargo-area/updateCargoArea")
    public JsonResult updateCargoArea(@RequestBody TCargoArea tCargoArea) {//修改数据
        itCargoAreaService.updateById(tCargoArea);
        return new JsonResult(1, "ok", tCargoArea);
    }


}
