package com.suyundeng.warehousemanage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.*;
import com.suyundeng.warehousemanage.service.ITCargoAreaService;
import com.suyundeng.warehousemanage.service.ITCargoService;
import com.suyundeng.warehousemanage.service.ITShelvesService;
import com.suyundeng.warehousemanage.service.ITWareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class TCargoController {


    @Autowired
    private ITCargoService itCargoService;

    @Autowired
    private ITWareService itWareService;

    @Autowired
    private ITCargoAreaService itCargoAreaService;

    @Autowired
    private ITShelvesService itShelvesService;
    /*
     * 查询所有的货位信息*/
    @RequestMapping(value = "/cargo",method = RequestMethod.GET)
    public JsonResult pageCargo(@RequestParam(value = "current",required = false,
            defaultValue = "1")Integer current){
        Page<TCargo> page = new Page<>(current, 5);
        IPage<TCargo> pageCargo = itCargoService.PageCargo(page);
        return new JsonResult(1,"ok",pageCargo);
    }

    /*添加货位*/
    @PostMapping("/cargo/add")
    public JsonResult addTCargo(@RequestBody TCargo cargo){
        int count = itCargoService.count();
        int nowCount=count+1;
        String id = "c"+nowCount;
        cargo.setId(id);
        LocalDateTime now = LocalDateTime.now();
        cargo.setCreatTime(Date.from(now.atZone(ZoneId.systemDefault()).toInstant()));
        itCargoService.save(cargo);
        return new JsonResult(1,"ok",cargo);
    }

    @PutMapping("/cargo")
    public JsonResult updateStatus(@RequestBody TCargo cargo){
        String status = cargo.getStatus();
        if (status.equals("有货")){
            cargo.setStatus("无货");
        }else {
            cargo.setStatus("有货");
        }
        itCargoService.updateById(cargo);
        return new JsonResult(1,"OK",cargo);
    }

    @PutMapping("/t-cargo/updateCargo")
    public JsonResult updateCargo(@RequestBody TCargo cargo){
        //修改仓库
        String wareName = cargo.getTWare().getWareName();
        TWare tWare = itWareService.getOne(new QueryWrapper<TWare>().lambda().eq(TWare::getWareName, wareName));
        //修改货区
        String goodsName = cargo.getTCargoArea().getGoodsName();
        TCargoArea cargoArea = itCargoAreaService.getOne(new QueryWrapper<TCargoArea>().lambda().eq(TCargoArea::getGoodsName, goodsName));
        String shelvesNo = cargo.getTShelves().getShelvesNo();
        TShelves shelves = itShelvesService.getOne(new QueryWrapper<TShelves>().lambda().eq(TShelves::getShelvesNo, shelvesNo));
        cargo.setWareId(tWare.getId());
        cargo.setDamId(cargoArea.getId());
        cargo.setShelvesId(shelves.getId());
        itCargoService.updateById(cargo);
        return new JsonResult(1,"ok",cargo);
    }


    /*条件查询*/
    @PostMapping("/cargo/select")
    public JsonResult selectCargo(@RequestBody TCargo cargo,
                                  @RequestParam(value = "current",required = false,
                                          defaultValue = "1")Integer current) {
        Page<TCargo> page = new Page<>(current, 5);
        if (cargo.getTShelves() != null) {
            String wareName = cargo.getTWare().getWareName();
            String goodsName = cargo.getTCargoArea().getGoodsName();
            String shelvesNo = cargo.getTShelves().getShelvesNo();
            TWare tWare = itWareService.getOne(new QueryWrapper<TWare>().lambda().eq(TWare::getWareName, wareName));
            TCargoArea cargoArea = itCargoAreaService.getOne(new QueryWrapper<TCargoArea>().lambda().eq(TCargoArea::getGoodsName, goodsName));
            TShelves shelves = itShelvesService.getOne(new QueryWrapper<TShelves>().lambda().eq(TShelves::getShelvesNo, shelvesNo));
            IPage<TCargo> cargos = itCargoService.findCargo(tWare.getId(),cargoArea.getId(),shelves.getId(),cargo.getCargoNo(),cargo.getStatus(),page);
            return new JsonResult(1, "ok", cargos);
        }else {
            IPage<TCargo> cargos = itCargoService.findCargo(null,null,null,cargo.getCargoNo(),cargo.getStatus(),page);
            return new JsonResult(1, "ok", cargos);
        }
    }

    /*查询货位数量*/
    @RequestMapping("/cargo/count")
    public JsonResult findCount(){
        int count = itCargoService.count();
        System.out.println("----"+count);
        return new JsonResult(1,"ok",count);
    }
}
