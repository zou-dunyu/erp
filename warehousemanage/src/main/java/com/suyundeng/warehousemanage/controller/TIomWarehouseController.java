package com.suyundeng.warehousemanage.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TIoWarehouse;
import com.suyundeng.entity.TIomWarehouse;
import com.suyundeng.warehousemanage.service.ITIoWarehouseService;
import com.suyundeng.warehousemanage.service.ITIomWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@RestController

public class TIomWarehouseController {

    @Autowired
    private ITIomWarehouseService iomWarehouseService;

    @Autowired
    private ITIoWarehouseService ioWarehouseService;

    @RequestMapping("/iomwarehouse/{ioNo}")
    public JsonResult selectIOM(@PathVariable String ioNo) {

//        TIomWarehouse byId = iomWarehouseService.getOne(new QueryWrapper<TIomWarehouse>().lambda()
//        .eq(TIomWarehouse::getIomNo,ioNo));
        List<TIomWarehouse> byId = iomWarehouseService.selectiomwh(ioNo);
        return new JsonResult(1, byId);

    }

    @PostMapping("/imoware")
    public JsonResult saveProduct(@RequestBody TIoWarehouse tIoWarehouse){


       tIoWarehouse.setCompanyId("2");
        System.out.println(tIoWarehouse+"-----------------------");

            tIoWarehouse.setId(UUID.randomUUID().toString().substring(0,10));
            ioWarehouseService.save(tIoWarehouse);
            return new JsonResult(1, tIoWarehouse);

    }

    @RequestMapping(value = "/iomwarehouse",method = RequestMethod.GET)
    public JsonResult pageProducts(@RequestParam(value = "current",required = false,defaultValue = "1")
                                           Integer current){

        IPage<TIomWarehouse> arg = new Page<>(current, 7);
        IPage<TIomWarehouse> page = iomWarehouseService.selectiomPage(arg);
        return new JsonResult(1, page);

    }

    // 按出入库单号和批次搜索
    @PostMapping("/search/{ioNo}/{batch}")
    public JsonResult search(@RequestParam(value = "current",required = false,defaultValue = "1")
                                     Integer current,
                             @PathVariable("ioNo")String ioNo,
                             @PathVariable("batch")Integer batch){
        System.out.println("current："+current);
        System.out.println("ioNo："+ioNo);
        System.out.println("batch："+batch);
        ioNo = "%"+ioNo+"%";
        String Batch = "%"+batch+"%";
        IPage<TIomWarehouse> arg = new Page<>(current, 7);
        IPage<TIomWarehouse> ioware = ioWarehouseService.search(ioNo,Batch,arg);
        return new JsonResult(1,ioware);
    }

    // 按出入库单号搜索
    @PostMapping("/searchIoNo/{ioNo}")
    public JsonResult searchIoNo(@RequestParam(value = "current",required = false,defaultValue = "1")
                                         Integer current,
                                 @PathVariable("ioNo")String ioNo){
        System.out.println("current："+current);
        System.out.println("ioNo："+ioNo);
        ioNo = "%"+ioNo+"%";
        String Batch = "%%";
        IPage<TIomWarehouse> arg = new Page<>(current, 7);
        IPage<TIomWarehouse> ioware = ioWarehouseService.search(ioNo,Batch,arg);
        return new JsonResult(1,ioware);
    }

    // 按批次搜索
    @PostMapping("/searchBatch/{batch}")
    public JsonResult searchBatch(@RequestParam(value = "current",required = false,defaultValue = "1")
                                          Integer current,
                                  @PathVariable("batch")Integer batch){
        System.out.println("current："+current);
        System.out.println("batch："+batch);
        String ioNo = "%%";
        String Batch = "%"+batch+"%";
        IPage<TIomWarehouse> arg = new Page<>(current, 7);
        IPage<TIomWarehouse> ioware = ioWarehouseService.search(ioNo,Batch,arg);
        return new JsonResult(1,ioware);
    }
}
