package com.suyundeng.warehousemanage.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TIomWarehouse;
import com.suyundeng.entity.TMoveWarehouse;
import com.suyundeng.warehousemanage.service.ITMoveWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@RestController

public class TMoveWarehouseController {

    @Autowired
    private ITMoveWarehouseService moveWarehouseService;

    @RequestMapping(value = "/moveware",method = RequestMethod.GET)
    public JsonResult pageProducts(@RequestParam(value = "current",required = false,defaultValue = "1")
                                           Integer current){

        IPage<TMoveWarehouse> arg = new Page<>(current, 7);
        IPage<TMoveWarehouse> page = moveWarehouseService.selectmovePage(arg);
        return new JsonResult(1, page);

    }
}
