package com.suyundeng.warehousemanage.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.*;
import com.suyundeng.warehousemanage.service.ITCargoAreaService;
import com.suyundeng.warehousemanage.service.ITShelvesService;
import com.suyundeng.warehousemanage.service.ITWareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TShelves;
import com.suyundeng.warehousemanage.service.ITShelvesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@RestController
public class TShelvesController {

    @Autowired
    private ITShelvesService itShelvesService;

    @Autowired
    private ITWareService itWareService;

    @Autowired
    private ITCargoAreaService itCargoAreaService;

    /*
    * 添加货架
    * */
    @PostMapping(value = "/shelf/add")
    public JsonResult addShelf(@RequestBody TShelves tShelves){
        tShelves.setStatus("0");
        int count = itShelvesService.count();
        int nowCount=count+1;
        String shelfId = "s"+nowCount;
        tShelves.setId(shelfId);
        LocalDateTime now = LocalDateTime.now();
        tShelves.setCreatTime(Date.from(now.atZone(ZoneId.systemDefault()).toInstant()));
        itShelvesService.save(tShelves);
        return new JsonResult(1,"yes",tShelves);
    }


    /*根据仓库名字查询该仓库下的所有库区*/
    @RequestMapping("/cargo/{wareName}")
    public JsonResult findCargoAresByWareId(@PathVariable("wareName") String wareName){
        TWare ware = itWareService.getOne(new QueryWrapper<TWare>().lambda()
                .eq(TWare::getWareName, wareName));
       List<TCargoArea> cargoAreas = itCargoAreaService.findCargoAresByWareId(ware.getId());
        return new JsonResult(1,"ok",cargoAreas);
    }

    /*
    * 查询所有的货架信息*/
    @RequestMapping(value = "/shelves",method = RequestMethod.GET)
    public JsonResult pageShelves(@RequestParam(value = "current",required = false,
            defaultValue = "1")Integer current){
        Page<TShelves> page = new Page<>(current, 5);
        IPage<TShelves>  pageShelves = itShelvesService.PageShelves(page);
        return new JsonResult(1,"ok",pageShelves);
    }

    /*
    *查询库区下的货架
    * */
    @RequestMapping("/shelves/{goodsName}")
        public JsonResult findShelvesByCargoAreaId(@PathVariable("goodsName") String goodsName){
        System.out.println(goodsName);
        TCargoArea tcargoarea = itCargoAreaService.getOne(new QueryWrapper<TCargoArea>().lambda()
                .eq(TCargoArea::getGoodsName, goodsName));
        List<TShelves> tShelves = itShelvesService.findShelvesNoByGoodsName(tcargoarea.getId());
        return new JsonResult(1,"",tShelves);
}

    /*修改货架状态
    * 已满 --> '1'
    * 未满-->'0'
    * */
    @PutMapping("/shelf")
    public JsonResult updateShelfStatus(@RequestBody TShelves tShelves){
        if ("1".equals(tShelves.getStatus())){
            tShelves.setStatus("0");
        }else{
            tShelves.setStatus("1");
        }
        itShelvesService.updateById(tShelves);
        return new JsonResult(1,"ok",tShelves);
    }

    //根据id查询货架
    @RequestMapping("/t-shelves/selectShelves/{id}")
    public JsonResult selectCargoArea(@PathVariable("id")String id){
        TShelves shelves=itShelvesService.selectCargoArea(id);
        return new JsonResult(1,"ok",shelves);
    }

    //查询所有货架
    @RequestMapping("/list/t-shelves")
    public JsonResult selectShelves(){
        List<TShelves> shelves=itShelvesService.list();
        return new JsonResult(1,"ok",shelves);
    }

  /*
  * 货架编辑框的数据修改
  * */
    @PutMapping("/t-shelves/updateShelves")
    public JsonResult updateShelves(@RequestBody TShelves tShelves){
        itShelvesService.updateById(tShelves);//修改货架
        return new JsonResult(1,"ok",tShelves);
    }

    /*
    * 根据仓库/货区/货架编号、时间来查询货架
    * */
    @RequestMapping("/t-shelves/{wareName}/{goodsName}/{shelvesNo}/{creatTime}")
    public JsonResult findByShelfAndCreateTime(@PathVariable("wareName") String wareName,
                                               @PathVariable("goodsName") String goodsName,
                                               @PathVariable("shelvesNo") String shelvesNo,
                                               @PathVariable("creatTime")String creatTime,
                                               @RequestParam(value = "current",required = false,
                                                       defaultValue = "1")Integer current) throws ParseException {
        Page<TShelves> page = new Page<>(current, 5);
        TWare ware = itWareService.getOne(new QueryWrapper<TWare>().lambda().eq(TWare::getWareName, wareName));
        TCargoArea tCargoArea = itCargoAreaService.getOne(new QueryWrapper<TCargoArea>().lambda().eq(TCargoArea::getGoodsName, goodsName));
        //将字符串转化为date类型，格式2016-10-12
        creatTime = creatTime.replace("GMT", "").replaceAll("\\(.*\\)", "");
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss z", Locale.ENGLISH);
        Date dateTrans = format.parse(creatTime);
        String selectCreatTime = new SimpleDateFormat("yyyyMMdd").format(dateTrans);
        /*查询符合条件的货架*/
        IPage<TShelves>  shelves = itShelvesService.findByWareAndCargoAndTime(ware.getId(),tCargoArea.getId(),shelvesNo,selectCreatTime,page);
        return new JsonResult(1,"ok",shelves);
    }

    /*根据时间查询货架*/
    @RequestMapping("/t-shelves/time/{creatTime}")
    public JsonResult findShelfByTime(@PathVariable("creatTime") String creatTime,
                                      @RequestParam(value = "current",required = false,
                                              defaultValue = "1")Integer current) throws ParseException {

        /*分页测试*/
        Page<TShelves> page = new Page<>(current, 5);

        //将字符串转化为date类型，格式2016-10-12
        creatTime = creatTime.replace("GMT", "").replaceAll("\\(.*\\)", "");
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss z", Locale.ENGLISH);
        Date dateTrans = format.parse(creatTime);
        String selectCreatTime = new SimpleDateFormat("yyyyMMdd").format(dateTrans);
        IPage<TShelves>  pageShelves = itShelvesService.findShelfByCreatTime(page,selectCreatTime);
        return new JsonResult(1,"ok",pageShelves);
    }

    /*根据仓库货区查询货架分页*/
    @RequestMapping("/t-shelves/{wareName}/{goodsName}/{shelvesNo}")
    public JsonResult findShelfByshelfNo(@PathVariable("wareName") String wareName,
                                         @PathVariable("goodsName") String goodsName,
                                         @PathVariable("shelvesNo") String shelvesNo,
                                         @RequestParam(value = "current",required = false,
                                                 defaultValue = "1")Integer current){
        Page<TShelves> page = new Page<>(current, 5);
        TWare ware = itWareService.getOne(new QueryWrapper<TWare>().lambda().eq(TWare::getWareName, wareName));
        TCargoArea tCargoArea = itCargoAreaService.getOne(new QueryWrapper<TCargoArea>().lambda().eq(TCargoArea::getGoodsName, goodsName));
        IPage<TShelves> shelves =  itShelvesService.findShelfByWareAndCarAndShelfNo(ware.getId(),tCargoArea.getId(),shelvesNo,page);
        return new JsonResult(1,"ok",shelves);
    }

    //根据库区id查询所有的货架
    @RequestMapping("/t-shelves/shelves/{cargoId}")
    public JsonResult findShelvesByCargoId(@PathVariable("cargoId")String cargoId){
        List<TShelves> shelves = itShelvesService.findShelvesNoByGoodsName(cargoId);
        return new JsonResult(1,"ok,",shelves);
    }

    /*查询货架数量*/
    @RequestMapping("/shelf/count")
    public JsonResult findCount(){
        int count = itShelvesService.count();
        return new JsonResult(1,"ok",count);
    }
}
