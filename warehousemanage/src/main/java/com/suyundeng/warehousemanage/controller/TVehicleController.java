package com.suyundeng.warehousemanage.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TVehicle;
import com.suyundeng.warehousemanage.service.ITVehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TVehicleController {

	@Autowired
	private ITVehicleService service;

	@GetMapping("/vehicle/logisticsID/{id}")
	public JsonResult findByLogisticsId(@PathVariable String id){
		List<TVehicle> list = service.list(new QueryWrapper<TVehicle>().lambda().eq(TVehicle::getLogisticsId, id));
		return new JsonResult(1,list);
	}
}
