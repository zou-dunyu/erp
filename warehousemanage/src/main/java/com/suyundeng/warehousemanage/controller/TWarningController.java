package com.suyundeng.warehousemanage.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TWarning;
import com.suyundeng.warehousemanage.service.ITWarningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@RestController
public class TWarningController {
    @Autowired
    private ITWarningService itWarningService;

    @GetMapping("/t-warning/selectList")
    public JsonResult selectAllWarning(@RequestParam(value = "current", required = false, defaultValue = "1") Integer current,
                                       @RequestParam("warehouseId") String id,
                                       @RequestParam("productName") String productName) {//查询预警表所有数据
        IPage<TWarning> page = new Page<>(current, 3);
        IPage<TWarning> warningData = itWarningService.selectAll_Warning(page, id, productName);
        System.out.println(warningData);
        return new JsonResult(1, "ok", warningData);
    }

    @PutMapping("/t-warning/update")
    public JsonResult update(@RequestBody TWarning tWarning) {//修改
        System.out.println(tWarning);
        itWarningService.updateByIdWarning(tWarning);
        return new JsonResult(1, "ok", tWarning);
    }

}
