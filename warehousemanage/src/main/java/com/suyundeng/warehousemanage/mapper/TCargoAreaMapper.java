package com.suyundeng.warehousemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TCargoArea;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TCargoAreaMapper  extends BaseMapper<TCargoArea> {

    IPage<TCargoArea> selectAllCargoArea(IPage<TCargoArea> page, @Param("tCargoArea") TCargoArea tCargoArea);//查询所有库区

    List<TCargoArea> selectCargoAresByWareId(String id);//根据仓库id查询库区
}
