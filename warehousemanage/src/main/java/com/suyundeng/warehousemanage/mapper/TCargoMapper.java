package com.suyundeng.warehousemanage.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.TCargo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
public interface TCargoMapper  extends BaseMapper<TCargo> {

    String selectCountEmpty(String id);//查询空闲货位

    TCargo getOneCargo(String cargoNo);

    /*查询货位表*/
    IPage<TCargo> selectAllCargo(Page<TCargo> page);

    /*条件查询1*/
    IPage<TCargo> selectCargo(@Param("wareId") String wareId,@Param("damId") String damId,
                              @Param("shelvesId") String shelvesId,@Param("cargoNo") String cargoNo,@Param("status") String status,
                              Page<TCargo> page);
}
