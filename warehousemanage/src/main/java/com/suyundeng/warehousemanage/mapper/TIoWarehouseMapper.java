package com.suyundeng.warehousemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TIoWarehouse;
import com.suyundeng.entity.TIomWarehouse;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Mapper
public interface TIoWarehouseMapper extends BaseMapper<TIoWarehouse> {

    IPage<TIoWarehouse> selectPage(IPage<TIoWarehouse> page);

    IPage<TIomWarehouse> search(@Param("ioNo") String ioNo,@Param("batch") String batch, IPage<TIomWarehouse> arg);
}
