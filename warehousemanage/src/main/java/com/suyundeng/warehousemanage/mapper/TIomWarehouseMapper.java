package com.suyundeng.warehousemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TIomWarehouse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Mapper
public interface TIomWarehouseMapper extends BaseMapper<TIomWarehouse> {

    List<TIomWarehouse> selectiomwh(String iomNo);

    IPage<TIomWarehouse> selectiomPage(IPage<TIomWarehouse> page);
}
