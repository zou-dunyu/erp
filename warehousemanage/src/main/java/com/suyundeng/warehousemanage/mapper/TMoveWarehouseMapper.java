package com.suyundeng.warehousemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TIoWarehouse;
import com.suyundeng.entity.TIomWarehouse;
import com.suyundeng.entity.TMoveWarehouse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Mapper
public interface TMoveWarehouseMapper extends BaseMapper<TMoveWarehouse> {

    IPage<TMoveWarehouse> selectmovePage(IPage<TMoveWarehouse> page);

    List<String> selectMvNo(String mvNo);

    List<TIomWarehouse> selectAllMove(String s);

    TMoveWarehouse getPerson(String mvNo);
}
