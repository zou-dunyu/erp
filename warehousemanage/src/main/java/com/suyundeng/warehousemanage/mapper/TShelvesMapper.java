package com.suyundeng.warehousemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.suyundeng.entity.TShelves;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Mapper
public interface TShelvesMapper extends BaseMapper<TShelves> {

    IPage<TShelves> selectAllShelvesPage(Page<TShelves> page);

    TShelves selectCargoArea(String id); //查询货架

    List<TShelves> selectShelvesNoByGoodsName(String id);//查询某个获取下的所有货架

    /*模糊查询*/
    IPage<TShelves> selectByWareAndCargoAndTime(@Param("wareId") String wareId, @Param("damId") String damId,
                                                @Param("shelvesNo") String shelvesNo,@Param("creatTime") String creatTime,
                                                Page<TShelves> page);

    IPage<TShelves> selectShelfByCreatTime(Page<TShelves> page,String creatTime);
/*条件查询*/
    IPage<TShelves> selectShelfByWareAndCarAndShelfNo(@Param("wareId") String wareId, @Param("damId") String damId,
                                                      @Param("shelvesNo") String shelvesNo,
                                                      Page<TShelves> page);
}
