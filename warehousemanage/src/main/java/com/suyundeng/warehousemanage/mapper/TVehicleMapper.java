package com.suyundeng.warehousemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.TVehicle;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TVehicleMapper extends BaseMapper<TVehicle> {
}
