package com.suyundeng.warehousemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyundeng.entity.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Mapper
public interface TWareMapper extends BaseMapper<TWare> {

    List<TWare> selectAllTWare();

    //根据出库名跟商品名查询产品
    List<MyProduct> selectMyProduct(@Param("outwareName") String outwareName, @Param("mvGoods") String mvGoods);//肖昊专用的别动
    //根据仓库名查询库区名
    List<String> selectgoodName(String ware);
    //根据库区名查询货架名
    List<TShelves> selectShelvesName(String goodName);
    //根据库区名查询货位
    List<TCargo> selectTCAName(String shelvesNo);

    TWare wareById(String id);//查询id对应仓库

    List<TWare> selectByIdByDate(@Param("id") String id, @Param("settledDate") String settledDate);//条件查询

    IPage<TWare> pagingSelectAll(IPage<TWare> page);//分页查询

    String selectStatus(String cargo_no);

    void updateTcStatus( @Param("status") String status,@Param("cargo_no") String cargo_no);

    TCargo getOneCargo(String cargoNo);//查询TcargoNO

    List<MyProduct> selectinWareNo(String inNo);

    List<MyProduct> selectinWareNo2(String inNo);
}
