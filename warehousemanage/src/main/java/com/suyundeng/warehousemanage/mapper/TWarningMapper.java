package com.suyundeng.warehousemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.suyundeng.entity.TWarning;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Mapper
public interface TWarningMapper extends BaseMapper<TWarning> {

    IPage<TWarning> selectAllWarning(IPage<TWarning> page,@Param("id") String id, @Param("productName") String productName); //查询预警表所有数据

    void updateByIdWarning(@Param("tWarning") TWarning tWarning);//修改预警值
}
