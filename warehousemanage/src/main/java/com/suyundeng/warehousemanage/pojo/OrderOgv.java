package com.suyundeng.warehousemanage.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class OrderOgv {
	private String id;

	private String orderId;

	private Integer number;

	private Double pickUpAmount;

	private Double money;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date dealTime;

	private String remark;

	private String allStatus;

	private String companyName;

	private String legal;

	private String companyAddress;

	private String pName;

	private String customId;

}
