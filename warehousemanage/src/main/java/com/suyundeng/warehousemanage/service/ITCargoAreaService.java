package com.suyundeng.warehousemanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TCargoArea;

import java.util.List;

public interface ITCargoAreaService extends IService<TCargoArea> {

    IPage<TCargoArea> selectAllCargoArea(IPage<TCargoArea> page, TCargoArea tCargoArea);//查询所有的库区表

    List<TCargoArea> findCargoAresByWareId(String id);//根据仓库id查询库区
}
