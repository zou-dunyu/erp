package com.suyundeng.warehousemanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TCargo;

import java.util.List;

public interface ITCargoService  extends IService<TCargo> {
    TCargo getOneCargo(String cargoNo);

    /*分页查询货位信息*/
    IPage<TCargo> PageCargo(Page<TCargo> page);
    /*分页条件查询*/
    IPage<TCargo> findCargo(String wareId, String damId, String shelvesId, String cargoNo, String status,Page<TCargo> page);
}
