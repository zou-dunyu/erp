package com.suyundeng.warehousemanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TIoWarehouse;
import com.suyundeng.entity.TIomWarehouse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
public interface ITIoWarehouseService extends IService<TIoWarehouse> {

    IPage<TIoWarehouse> pageRoles(IPage<TIoWarehouse> page);

    IPage<TIomWarehouse> search(String ioNo, String batch, IPage<TIomWarehouse> arg);
}
