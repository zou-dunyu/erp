package com.suyundeng.warehousemanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TIomWarehouse;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
public interface ITIomWarehouseService extends IService<TIomWarehouse> {

    List<TIomWarehouse> selectiomwh(String iomNo);

    IPage<TIomWarehouse> selectiomPage(IPage<TIomWarehouse> page);

}
