package com.suyundeng.warehousemanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TIomWarehouse;
import com.suyundeng.entity.TMoveWarehouse;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
public interface ITMoveWarehouseService extends IService<TMoveWarehouse> {

    IPage<TMoveWarehouse> selectmovePage(IPage<TMoveWarehouse> page);


    List<String> selectMvNo(String mvNo);

    List<TIomWarehouse> selectAllMove(String s);

    TMoveWarehouse getPerson(String mvNo);

}
