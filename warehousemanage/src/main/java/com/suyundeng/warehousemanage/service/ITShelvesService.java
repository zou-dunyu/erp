package com.suyundeng.warehousemanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TShelves;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
public interface ITShelvesService extends IService<TShelves> {

    IPage<TShelves> PageShelves(Page<TShelves> page);


    TShelves selectCargoArea(String id);//查询货架

    List<TShelves> findShelvesNoByGoodsName(String id);//查询某个货区下的货架

    /*模糊查询*/
    IPage<TShelves> findByWareAndCargoAndTime(String id, String id1,String shelvesNo, String selectCreatTime, Page<TShelves> page);

    /*根据时间模糊查询1*/
    IPage<TShelves> findShelfByCreatTime(Page<TShelves> page,String selectCreatTime);

    IPage<TShelves> findShelfByWareAndCarAndShelfNo(String wareId, String damId, String shelvesNo, Page<TShelves> page);
}
