package com.suyundeng.warehousemanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TVehicle;

public interface ITVehicleService extends IService<TVehicle> {
}
