package com.suyundeng.warehousemanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.*;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
public interface ITWareService extends IService<TWare> {

    public List<TWare> findAllTWare();

    List<MyProduct> selectMyProduct(String outwareName, String mvGoods);//肖昊专用的别动.

    List<String> selectgoodName(String ware);


    TWare wareById(String id); //查询id对应仓库

    //根据库区名查询货架名
    List<TShelves> selectShelvesName(String goodName);
    //根据库区名查询货位
    List<TCargo> selectTCAName(String shelvesNo);

    //查询是够有货
    String selectStatus(String cargo_no);

    //修改TC有货状态
    void updateTcStatus(String status,String cargo_no);

    //根据订单id来查询入库订单
    List<MyProduct> selectinWareNo(String inNo);

    List<MyProduct> selectinWareNo2(String inNo);


    List<TWare> selectByIdByDate(String id, String settledDate);//条件查询数据

    IPage<TWare> pagingSelectAllWare(IPage<TWare> page); //分页查询
}
