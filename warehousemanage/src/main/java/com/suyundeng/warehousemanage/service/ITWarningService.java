package com.suyundeng.warehousemanage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.suyundeng.entity.TWarning;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
public interface ITWarningService extends IService<TWarning> {

    IPage<TWarning> selectAll_Warning(IPage<TWarning> page, String id, String productName); //查询预警表所有数据

    void updateByIdWarning(TWarning tWarning);//修改预警值
}
