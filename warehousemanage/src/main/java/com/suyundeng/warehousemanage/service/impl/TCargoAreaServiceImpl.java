package com.suyundeng.warehousemanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TCargoArea;
import com.suyundeng.warehousemanage.mapper.TCargoAreaMapper;
import com.suyundeng.warehousemanage.service.ITCargoAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TCargoAreaServiceImpl extends ServiceImpl<TCargoAreaMapper, TCargoArea> implements ITCargoAreaService {

    @Autowired
    private TCargoAreaMapper tCargoAreaMapper;

    @Override
    public IPage<TCargoArea> selectAllCargoArea(IPage<TCargoArea> page,TCargoArea tCargoArea) {
        return tCargoAreaMapper.selectAllCargoArea(page,tCargoArea);
    }


    @Override
    public List<TCargoArea> findCargoAresByWareId(String id) {
        return tCargoAreaMapper.selectCargoAresByWareId(id);
    }

}
