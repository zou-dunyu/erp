package com.suyundeng.warehousemanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TCargo;
import com.suyundeng.warehousemanage.mapper.TCargoMapper;
import com.suyundeng.warehousemanage.service.ITCargoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TCargoServiceImpl extends ServiceImpl<TCargoMapper, TCargo> implements ITCargoService {

    @Autowired
    private TCargoMapper mapper;

    @Override
    public TCargo getOneCargo(String cargoNo) {
        return mapper.getOneCargo(cargoNo);
    }

    @Override
    public IPage<TCargo> PageCargo(Page<TCargo> page) {
        IPage<TCargo> tCargoIPage = mapper.selectAllCargo(page);
        return tCargoIPage;
    }

    /*
    * 条件查询分页
    * */
    @Override
    public IPage<TCargo> findCargo(String wareId, String damId, String shelvesId, String cargoNo, String status,Page<TCargo> page) {
        return mapper.selectCargo(wareId,damId,shelvesId,cargoNo,status,page);
    }
}
