package com.suyundeng.warehousemanage.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TIoWarehouse;
import com.suyundeng.entity.TIomWarehouse;
import com.suyundeng.warehousemanage.mapper.TIoWarehouseMapper;
import com.suyundeng.warehousemanage.service.ITIoWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Service
public class TIoWarehouseServiceImpl extends ServiceImpl<TIoWarehouseMapper, TIoWarehouse> implements ITIoWarehouseService {

    @Autowired
    private TIoWarehouseMapper tIoWarehouseMapper;

    @Override
    public IPage<TIoWarehouse> pageRoles(IPage<TIoWarehouse> page) {
        return tIoWarehouseMapper.selectPage(page);
    }

    @Override
    public IPage<TIomWarehouse> search(String ioNo, String batch, IPage<TIomWarehouse> arg) {
        return tIoWarehouseMapper.search(ioNo,batch,arg);
    }
}
