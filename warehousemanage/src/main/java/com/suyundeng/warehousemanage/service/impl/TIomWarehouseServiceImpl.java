package com.suyundeng.warehousemanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TIomWarehouse;
import com.suyundeng.warehousemanage.mapper.TIomWarehouseMapper;
import com.suyundeng.warehousemanage.service.ITIomWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Service
public class TIomWarehouseServiceImpl extends ServiceImpl<TIomWarehouseMapper, TIomWarehouse> implements ITIomWarehouseService {

    @Autowired
    private TIomWarehouseMapper iomWarehouse;

    @Override
    public List<TIomWarehouse> selectiomwh(String iomNo) {
        return iomWarehouse.selectiomwh(iomNo);
    }

    @Override
    public IPage<TIomWarehouse> selectiomPage(IPage<TIomWarehouse> page) {
        return iomWarehouse.selectiomPage(page);
    }
}
