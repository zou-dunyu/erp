package com.suyundeng.warehousemanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TIomWarehouse;
import com.suyundeng.entity.TMoveWarehouse;
import com.suyundeng.warehousemanage.mapper.TMoveWarehouseMapper;
import com.suyundeng.warehousemanage.service.ITMoveWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Service
public class TMoveWarehouseServiceImpl extends ServiceImpl<TMoveWarehouseMapper, TMoveWarehouse> implements ITMoveWarehouseService {

    @Autowired
    private TMoveWarehouseMapper moveWarehouseMapper;

    @Override
    public IPage<TMoveWarehouse> selectmovePage(IPage<TMoveWarehouse> page) {
        return moveWarehouseMapper.selectmovePage(page);
    }

    @Override
    public List<String> selectMvNo(String mvNo) {
        return moveWarehouseMapper.selectMvNo(mvNo);
    }

    @Override
    public List<TIomWarehouse> selectAllMove(String s) {
        return moveWarehouseMapper.selectAllMove(s);
    }

    @Override
    public TMoveWarehouse getPerson(String mvNo) {
        return moveWarehouseMapper.getPerson(mvNo);
    }
}
