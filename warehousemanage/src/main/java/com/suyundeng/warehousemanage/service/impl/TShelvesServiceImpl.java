package com.suyundeng.warehousemanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.JsonResult;
import com.suyundeng.entity.TShelves;
import com.suyundeng.warehousemanage.mapper.TCargoMapper;
import com.suyundeng.warehousemanage.mapper.TShelvesMapper;
import com.suyundeng.warehousemanage.service.ITShelvesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Service
public class TShelvesServiceImpl extends ServiceImpl<TShelvesMapper, TShelves> implements ITShelvesService {


    @Autowired
    private TShelvesMapper tShelvesMapper;

    @Autowired
    private TCargoMapper tCargoMapper;

    @Override
    public IPage<TShelves> PageShelves(Page<TShelves> page) {
        IPage<TShelves> tShelvesIPage = tShelvesMapper.selectAllShelvesPage(page);
        List<TShelves> tShelves = tShelvesIPage.getRecords();
        Iterator<TShelves> iterator = tShelves.iterator();
        while (iterator.hasNext()){//赋默认值，有货/无货
            TShelves next = iterator.next();
            String id = next.getId();
            String count = tCargoMapper.selectCountEmpty(id);
            if (count == null){
                count = "0";
            }
            next.setEmptySpace(count);
        }
        return tShelvesIPage;
    }

    @Override
    public TShelves selectCargoArea(String id) {//查询货架
        return tShelvesMapper.selectCargoArea(id);
    }

    @Override
    public List<TShelves> findShelvesNoByGoodsName(String id) {
        return tShelvesMapper.selectShelvesNoByGoodsName(id);
    }

    @Override
    public IPage<TShelves> findByWareAndCargoAndTime(String wareId, String damId,String shelvesNo, String creatTime,Page<TShelves> page) {
        return tShelvesMapper.selectByWareAndCargoAndTime(wareId,damId,shelvesNo,creatTime,page);
    }

    @Override
    public IPage<TShelves> findShelfByCreatTime(Page<TShelves> page,String selectCreatTime) {
        return tShelvesMapper.selectShelfByCreatTime(page,selectCreatTime);
    }

    /*
    * 分页条件查询
    * */
    @Override
    public IPage<TShelves> findShelfByWareAndCarAndShelfNo(String wareId, String damId, String shelvesNo,Page<TShelves> page) {
        return tShelvesMapper.selectShelfByWareAndCarAndShelfNo(wareId,damId,shelvesNo,page);
    }
}
