package com.suyundeng.warehousemanage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TVehicle;
import com.suyundeng.warehousemanage.mapper.TVehicleMapper;
import com.suyundeng.warehousemanage.service.ITVehicleService;
import org.springframework.stereotype.Service;

@Service
public class TVehicleServiceImpl extends ServiceImpl<TVehicleMapper, TVehicle> implements ITVehicleService {
}
