package com.suyundeng.warehousemanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.*;
import com.suyundeng.entity.MyProduct;
import com.suyundeng.entity.TCargoArea;
import com.suyundeng.entity.TWare;
import com.suyundeng.warehousemanage.mapper.TCargoMapper;
import com.suyundeng.warehousemanage.mapper.TWareMapper;
import com.suyundeng.warehousemanage.service.ITWareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Service
public class TWareServiceImpl extends ServiceImpl<TWareMapper, TWare> implements ITWareService {
    @Autowired
    private TWareMapper mapper;
    @Autowired
    private TCargoMapper tCargoMapper;

    public List<TWare> findAllTWare() {
        return mapper.selectAllTWare();
    }

    @Override
    public List<MyProduct> selectMyProduct(String outwareName, String mvGoods) {
        return mapper.selectMyProduct(outwareName, mvGoods);
    }

    @Override
    public List<String> selectgoodName(String ware) {
        return mapper.selectgoodName(ware);
    }

    @Override
    public TWare wareById(String id) {//查询id对应仓库
        return mapper.wareById(id);
    }

    @Override
    public List<TShelves> selectShelvesName(String goodName) {
        return mapper.selectShelvesName(goodName);
    }

    @Override
    public List<TCargo> selectTCAName(String shelvesNo) {
        return mapper.selectTCAName(shelvesNo);
    }

    @Override
    public List<TWare> selectByIdByDate(String id, String settledDate) {//条件查询
        return mapper.selectByIdByDate(id, settledDate);
    }

    @Override
    public IPage<TWare> pagingSelectAllWare(IPage<TWare> page) {//分页查询
        IPage<TWare> tWareIPage = mapper.pagingSelectAll(page);
      /*  for (TWare record : tWareIPage.getRecords()) {
            record.setVacantSpace(tCargoMapper.selectCountEmpty(record.getSId()));//查询空货位，并赋值
        }*/
        return tWareIPage;
    }

    @Override
    public String selectStatus(String cargo_no) {
        return mapper.selectStatus(cargo_no);
    }

    @Override
    public void updateTcStatus(String status, String cargo_no) {
        mapper.updateTcStatus(status, cargo_no);
    }

    @Override
    public List<MyProduct> selectinWareNo(String inNo) {
        return mapper.selectinWareNo(inNo);
    }

    @Override
    public List<MyProduct> selectinWareNo2(String inNo) {
        return mapper.selectinWareNo(inNo);
    }


}
