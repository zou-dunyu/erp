package com.suyundeng.warehousemanage.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyundeng.entity.TWarning;
import com.suyundeng.warehousemanage.mapper.TWarningMapper;
import com.suyundeng.warehousemanage.service.ITWarningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author xh
 * @since 2021-04-19
 */
@Service
public class TWarningServiceImpl extends ServiceImpl<TWarningMapper, TWarning> implements ITWarningService {

    @Autowired
    private TWarningMapper tWarningMapper;

    @Override
    public IPage<TWarning> selectAll_Warning(IPage<TWarning> page, String id, String productName) {//查询预警表所有数据
        IPage<TWarning> tWarnings = tWarningMapper.selectAllWarning(page, id, productName);
        return tWarnings;
    }

    @Override
    public void updateByIdWarning(TWarning tWarning) {   //修改预警值
        tWarningMapper.updateByIdWarning(tWarning);
    }
}
